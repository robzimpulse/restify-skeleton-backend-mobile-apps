var restify = require('restify');
var util = require('util');
var fs = require('fs');
var magicGlobals = require('magic-globals');
var mongoose = require('mongoose');
var logger = require('restify-logger');
var autoIncrement = require('mongoose-auto-increment');
var config = require(__base+'/app/config');

logger.format('my-simple-format', ':method | :date[web] | :status | :response-time ms | :url ');


// Error
function MyError(message) {
  restify.RestError.call(this, {
    restCode: 'MyError',
    statusCode: 418,
    message: message,
    constructorOpt: MyError
  });
  this.name = 'MyError';
}
util.inherits(MyError, restify.RestError);

// Server
var server = restify.createServer({
  certificate: fs.readFileSync('openssl/server.crt'),
  key: fs.readFileSync('openssl/server.key'),
  ca: fs.readFileSync('openssl/ca.crt'),
  name: 'MasakuService'
});

// Prevent crashing from uncaught error
var domain = require('domain').create();
domain.on('error', function (err) {
  console.error(err);
});

// Connect to mongodb
mongoose.connect(config.database.host, {
  server: {
    socketOptions: {
      keepAlive: 1
    }
  },
  user: config.database.user,
  pass: config.database.password
});
var db = mongoose.connection;
autoIncrement.initialize(db);

// Localizations
global.i18n = new (require('i18n-2'))({
  locales: ['id', 'en'],
  directory: __base+'/app/langs'
});

// Plugins
server
  .use(restify.gzipResponse())
  .use(restify.queryParser())
  .use(restify.bodyParser({maxBodySize: config.image.fileSize,mapParams:false}))
  .use(logger('my-simple-format'));
  /*
  .use(logger('custom', {
      skip: function (req) {
      return process.env.NODE_ENV === "test" || req.method === "OPTIONS" || req.url === "/status";
    }
  }));
  */

// Controllers
var controllers = {};
var controllers_path = process.cwd() + '/app/controllers';
fs.readdirSync(controllers_path).forEach(function (file) {
  if (file.indexOf('.js') != -1) {
    controllers[file.split('.')[0]] = require(controllers_path + '/' + file);
  }
});

// Middlewares
var middlewares = {};
var middlewares_path = process.cwd() + '/app/middlewares';
fs.readdirSync(middlewares_path).forEach(function (file) {
  if (file.indexOf('.js') != -1) {
    middlewares[file.split('.')[0]] = require(middlewares_path + '/' + file);
  }
});

// Static
server.get(/\/admin\/?.*/, restify.serveStatic({
  directory: './public',
  default: 'index.html'
}));
server.get(/\/images\/?.*/, restify.serveStatic({
  directory: './public',
  default: 'index.html'
}));

// Routes
//require('./app/routes.js')(server, middlewares, controllers);

// Execute this file on server restart
//require('./app/boot.js');

// DB Seeds
//require('./app/seed.js');

server.listen(config.port);
console.log("Starting server at port : "+config.port+" ["+new Date(Date.now())+"]");