var env = require(__base+'/app/env');

var config = {
  secret: 'A4H23ZpO50mQgVbSfsS45awAJk5aO8GS',
  saltFactor: 10,
  maxMultiDevice: 5,
  database: {
    host: 'mongodb://localhost/ticklog',
    user: 'ticklog',
    password: 'kiasu123'
  },
  userApiToken: "'4Os4T9N388'CL1K>19@d,%OZB&@D9",
  adminApiToken: "7i.7U2/LB,+H{WKXsx011n[&9oJ$&h",
  tokenExpiration: '30days',
  image: {
    url: env.hostname+'/images/',
    width: 550,   //px
    height: 550,  //px
    quality: 100,
    fileSize: 5000000 //bytes
  },
  notificationMessage: {
    verification: 'Your verification code is : ',
    resetPassword: 'Your reset password code is : '
  }
};

if (env.stage == "development") {
  config.serverIp = '127.0.0.1';
  config.port = 2053;
  config.database = {
    host: env.database.host,
    user: env.database.user,
    password: env.database.password
  };
}

module.exports = config;