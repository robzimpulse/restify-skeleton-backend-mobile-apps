var jwt = require('jsonwebtoken');

var config = require(__base + '/app/config');

var controller = {

  getAppToken: function (req, res, next) {

  	var userToken = jwt.sign({ apiToken: config.userApiToken }, config.secret);
    var adminToken = jwt.sign({ apiToken: config.adminApiToken }, config.secret);
    
    res.send({
      userToken: userToken,
      adminToken: adminToken
    });

    return next();

  }

};

module.exports = controller;