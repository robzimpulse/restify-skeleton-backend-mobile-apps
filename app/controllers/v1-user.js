var restify = require('restify');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require(__base + '/app/config');
var phoneNumberFilter = require(__base+'/app/helpers/phone-number-filter');
var mongooseError = require(__base+'/app/helpers/mongoose-error');
var User = require(__base+'/app/models/user');
var SALT_WORK_FACTOR = 10;

var controller = {

  postRegister: function (req, res, next) {

    var fname = req.body.firstName;
    var lname = req.body.lastName;
    var gender = req.body.gender;
    var dob = parseInt(req.body.dob);
    var mob = parseInt(req.body.mob)-1;
    var yob = parseInt(req.body.yob);
    var phoneNumber = phoneNumberFilter(req.body.phoneNumber);
    var password = req.body.password;
    var verificationCode = User.generateVerificationCode();
    var email = req.body.email;

    if (!dob) {
      dob = 1;
    }
    if(!mob){
      mob = 0;
    }
    if(!yob){
      yob = 1990;
    }

    var data = {
      local:{
        firstName: fname.toUpperCase(),
        lastName: lname.toUpperCase(),
        email: email,
        password: password,
        phoneNumber: phoneNumber
      },
      profile:{
        gender: gender,
        dateOfBirth: new Date(yob,mob,dob)
      },
      verificationCode: verificationCode
    };

    User.create(data, function (err, user) {
      if (err) {
        return next(new mongooseError(err));
      }

      // Sign Token
      var token = jwt.sign({ userId: user._id }, config.secret, {
        expiresIn: config.tokenExpiration
      });
      user.refreshTokens.push(token);
      user.save();

      user.sendSMSVerificationCode(function (err, res) {
        console.log('send sms verificationCode err:', err);
        console.log('send sms verificationCode res:', res);
      });

      user = user.toObject();

      delete user['local']['password'];
      delete user['verificationCode'];
      delete user['refreshTokens'];
      delete user['deviceTokens'];

      res.send(201, {
        user: user,
        token: token
      });

      return next();
    });
  },

  postAuthenticate: function (req, res, next) {

    var email = req.body.email;
    var password = req.body.password;

    User.findOne({ 'local.email': email }, '+local.password +refreshTokens', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('Anda belum terdaftar')));
      }

      user.comparePassword(password, function (err, isMatch) {
        if (!isMatch) {
          return next(new restify.UnauthorizedError(i18n.__('Password yang anda masukkan salah')));
        }

        // Sign Token
        var token = jwt.sign({ userId: user._id }, config.secret, {
          expiresIn: config.tokenExpiration // expires in seconds
        });

        if (user.refreshTokens.length == config.maxMultiDevice) {
          user.refreshTokens.pull(user.refreshTokens[0]);
        }

        if (user.refreshTokens.indexOf(token) == -1) {
          user.refreshTokens.push(token);
        }

        user.save();

        user = user.toObject();
        delete user['local']['password'];
        delete user['refreshTokens'];

        res.send({
          user: user,
          token: token
        });

        return next();
      });

    });
  },

  postRefreshToken: function (req, res, next) {

    var expiredToken = req.body.token;

    if (!expiredToken) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    jwt.verify(expiredToken, config.secret, { ignoreExpiration: true }, function (err, decoded) {
      if (err) {
        return next(new restify.UnauthorizedError(i18n.__('Invalid Token')));
      }

      if (!decoded.userId) {
        return next(new restify.ForbiddenError(i18n.__('Akses ditolak')));
      }

      User.findOne({
        _id: decoded.userId,
        refreshTokens: expiredToken
      }, '+refreshTokens', function (err, user) {
        if (err) {
          return next(new restify.InternalServerError(500));
        }

        if (user == null) {
          return next(new restify.NotFoundError(i18n.__('Tidak ditemukan')));
        }

        var token = jwt.sign({ userId: decoded.userId }, config.secret, {
          expiresIn: config.tokenExpiration
        });

        if (typeof user.refreshTokens === 'undefined') {
          user.refreshTokens = [];
        } else {
          user.refreshTokens.pull(expiredToken);
        }

        user.refreshTokens.push(token);
        user.save();

        res.send({
          token: token
        });

        return next();
      });

    });

  },

  postLogout: function (req, res, next) {

    var token = req.headers['x-access-token'];
    var deviceToken = req.body.deviceToken;
    var userId = req.decoded.userId;

    var pull = { refreshTokens: token };

    if (deviceToken) {
      pull.deviceTokens = deviceToken;
    }

    User.update({ _id: userId }, { $pull: pull }, function (err) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      res.send(true);
      return next();
    });

  },

  postLogoutAll: function (req, res, next) {

    var userId = req.decoded.userId;

    User.update({ _id: userId }, { $set: { logoutTime: new Date(), refreshTokens: [], deviceTokens: [] } }, function (err) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      res.send(true);
      return next();
    });

  },

  postResendCode: function (req, res, next) {

    var userId = req.decoded.userId;

    User.findById(userId, '+verificationCode', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.NotFoundError(i18n.__('Tidak ditemukan')));
      }

      if (user.verificationCode) {

        user.sendSMSVerificationCode(function(err,res){
          if(err)
            console.log(err);
          if(res)
            console.log(res);
        });
        res.json(true);
        return next();
      }

      return next(new restify.BadRequestError());
    });

  },

  postVerify: function (req, res, next) {

    var userId = req.decoded.userId;
    var verificationCode = req.body.verificationCode;

    if (!verificationCode) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    User.findById(userId, '+verificationCode', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.NotFoundError(i18n.__('Tidak ditemukan')));
      }

      user.verify(verificationCode, function (err) {
        if (err) {
          return next(new restify.UnauthorizedError(err));
        }

        res.json(true);
        return next();
      });
    });

  },

  postDeviceToken: function (req, res, next) {

    var deviceToken = req.body.deviceToken;

    if (!deviceToken) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    var userId = req.decoded.userId;

    User.findById(userId, 'deviceTokens', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user.deviceTokens.length == config.maxMultiDevice) {
        user.deviceTokens.pull(user.deviceTokens[0]);
      }

      if (user.deviceTokens.indexOf(deviceToken) == -1) {
        user.deviceTokens.push(deviceToken);
      }

      user.save(function (err) {
        if (err) {
          return next(new restify.InternalServerError(500));
        }

        res.send(true);
        return next();
      });
    });

  },

  deleteDeviceToken: function (req, res, next) {

    var deviceToken = req.params.token;

    if (!deviceToken) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    var userId = req.decoded.userId;

    User.update({ _id: userId }, { $pull: { deviceTokens: deviceToken }}, function (err) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      res.send(true);
      return next();
    });

  },

  postUpdatePassword: function ( req, res, next ) {
    var userId = req.decoded.userId;
    var newPassword = req.body.newPassword;
    var oldPassword = req.body.oldPassword;

    User.findOne({ _id: userId }, '+password', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('Password yang anda masukkan salah')));
      }

      user.comparePassword(oldPassword, function (err, isMatch) {
        if (!isMatch) {

          return next(new restify.UnauthorizedError(i18n.__('Password lama tidak cocok')));
        }

        var newHashPass = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(SALT_WORK_FACTOR));
        User.update({ _id: userId },{ password : newHashPass },function(err){
          if (err) {
            return next(new restify.InternalServerError(500));
          }

          res.send(true);

          return next();
        });
      });
    });
  },



  getProfile: function (req, res, next){
    var userId = req.params.id;
    var select = '';
    if(!userId){
      userId = req.decoded.userId;
      select = '-asHelper -asRequestor';
    }

    User.findOne({ _id: userId }, select, function(err,user){
      if (err) {
        return next(new mongooseError(err));
      }
      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('User tidak terdaftar')));
      }
      res.send(user);
      return next();
    });

  },

  putUpdateProfile: function (req, res, next) {
    var displayName = req.body.displayName.toUpperCase();
    var bio = req.body.bio;
    var address = req.body.address;
    var userId = req.decoded.userId;
    var dob = parseInt(req.body.dob);
    var mob = parseInt(req.body.mob)-1;
    var yob = parseInt(req.body.yob);
    var phoneNumber = req.body.phoneNumber;
    var gender = req.body.gender;

    if(!dob){dob = 1;}
    if(!mob){mob = 0;}
    if(!yob){yob = 1990;}

    User.findOne({ _id: userId },function(err,user){
      if (err) {
        return next(new mongooseError(err));
      }
      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('User tidak terdaftar')));
      }

      if(displayName){user.profile.displayName = displayName;}
      if(bio){user.profile.bio = bio;}
      if(address){user.profile.address = address;}
      if(phoneNumber){user.profile.phoneNumber = phoneNumber;}
      if(gender){user.profile.gender = gender;}
      user.profile.dateOfBirth = new Date(yob,mob,dob);
      multiplePhoto(req,user.profile.displayName.replace(' ','-'),0).forEach(function(photo){
        user.profile.images.push(photo);
      });
      user.save();
      res.send(true);
      return next();
    });

  },

  postFeedback: function (req, res, next) {
    var userId = req.params.id;
    var from = req.decoded.userId;    //from
    var location = req.body.location; //location
    var rating = req.body.rating;     //rating
    var title = req.body.title;       //title
    var message = req.body.message;   //message
    var role = req.body.role;

    var feedback = {
      from: from,
      title: title,
      message: message,
      location: location,
      rating: rating
    };

    User.findOne({ _id: userId }, function(err,user){
      if (err) {
        return next(new mongooseError(err));
      }
      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('User tidak terdaftar')));
      }
      if(role == 'helper'){
        user.asHelper.feedbacks.push(feedback);
      }else{
        user.asRequestor.feedbacks.push(feedback);
      }
      user.save();
      res.send(true);
    });

  },

  deleteFeedback: function (req, res, next){
    var userId = req.params.id;
    var feedId = req.params.fid;
    var fromId = req.decoded.userId;
    var role = req.body.role;

    User.findOne({ _id: userId }, function(err,user){
      if (err) {
        return next(new mongooseError(err));
      }
      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('User tidak terdaftar')));
      }
      if(role == 'helper'){
        user.asHelper.feedbacks.forEach(function(feedback){
          if(feedId == feedback._id && fromId == feedback.from){
            user.asHelper.feedbacks.pull(feedback);
          }
        });
      }else{
        user.asRequestor.feedbacks.forEach(function(feedback){
          if(feedId == feedback._id && fromId == feedback.from){
            user.asRequestor.feedbacks.pull(feedback);
          }
        });
      }
      user.save();
      res.send(true);
    });

  }

};

function multiplePhoto(req, suffix, count){
  var myObject = req.files;
  var images = [];
  for (var name in myObject) {
    if (myObject.hasOwnProperty(name)) {
      count++;
      images.push(savePhoto(myObject[name],suffix+'-'+name+'-'+count));
    }
  }
  return images;
}

function savePhoto(objectFiles,filename){
  gm(objectFiles.path)
      .resize(config.image.width, config.image.height, '!')
      .quality(config.image.quality)
      .write(config.image.menu.path + filename + '.jpg', function (err) {
        if (err) {
          console.log(err);
          return next(new restify.InternalServerError(500));
        }
      });
  return filename + '.jpg';
}

module.exports = controller;