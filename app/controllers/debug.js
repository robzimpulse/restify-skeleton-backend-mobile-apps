var controller = {


    getFacebook: function (req, res, next) {
        res.send(req.user);
        return next();
    },

    displayForm: function (req, res, next) {

        res.writeHead(200, {'content-type': 'text/html'});
        res.end(
            '<form action="/" enctype="multipart/form-data" method="post">'+
            '<input type="text" name="id"><br>'+
            '<input type="file" name="upload" multiple="multiple"><br>'+
            '<input type="submit" value="Upload">'+
            '</form>'
        );
        return next();
    }

};

module.exports = controller;