var restify = require('restify');
var crypto = require('crypto');

var config = require(__base+'/app/config');
var phoneNumberFilter = require(__base+'/app/helpers/phone-number-filter');
var User = require(__base+'/app/models/user');

var controller = {

  postForgot: function (req, res, next) {

    var phoneNumber = phoneNumberFilter(req.body.phoneNumber);

    if (!phoneNumber) {
    	return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    User.findOne({ phoneNumber: phoneNumber }, function (err, user) {
      if (err) {
      	return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('%s tidak ditemukan', 'User')));
      }

      crypto.randomBytes(20, function(err, buf) {
        var resetPasswordToken = buf.toString('hex').substring(0, config.reminder.tokenLength);
        
        user.resetPasswordToken = {
          token: resetPasswordToken,
          expiration: Date.now() + 3600000 // 1 hour
        };

        user.save(function (err) {
          if (err) {
            return next(new restify.InternalServerError(500));
          }

          user.sendSMSResetPasswordToken(function (err) {
            console.log('Send SMS Reset Password Token Error:', err);
          });

          res.send(true);
          return next();
        });
      });

    });

  },

  postResendToken: function (req, res, next) {

    var phoneNumber = phoneNumberFilter(req.body.phoneNumber);

    if (!phoneNumber) {
    	return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    User.findOne({ phoneNumber: phoneNumber }, function (err, user) {
      if (err) {
      	return next(new restify.InternalServerError(500));
      }

      user.sendSMSResetPasswordToken(function (err) {
        console.log('Resend SMS Reset Password Token Error:', err);
      });

      res.send(true);
      return next();
    });

  },

  postCheck: function (req, res, next) {

    var phoneNumber = phoneNumberFilter(req.body.phoneNumber);
    var token = req.body.token;

    if (!(phoneNumber && token)) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    User.findOne({ phoneNumber: phoneNumber, 'resetPasswordToken.token': token }, function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('Token tidak cocok')));
      }

      res.send(true);
      return next();
    });

  },

  postReset: function (req, res, next) {

    var phoneNumber = phoneNumberFilter(req.body.phoneNumber);
    var token = req.body.token;
    var password = req.body.password;

    if (!(phoneNumber && token && password)) {
      return next(new restify.InvalidArgumentError(i18n.__('Invalid Argument')));
    }

    User.findOne({ phoneNumber: phoneNumber, 'resetPasswordToken.token': token }, function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.UnauthorizedError(i18n.__('%s tidak ditemukan', 'User')));
      }

      user.password = password;
      user.resetPasswordToken = undefined;
      user.save(function (err) {
        if (err) {
          return next(new restify.InternalServerError(500));
        }

        res.send(true);
      });

    });

  }

};

module.exports = controller;