var jwt = require('jsonwebtoken');

var config = require(__base + '/app/config');
var mongooseError = require(__base+'/app/helpers/mongoose-error');
var User = require(__base+'/app/models/user');

var controller = {

    postRegister: function (req, res, next) {
        var data = {
            local:{},
            profile:{},
            core: {}
        };

        var phoneNumber = req.body.phoneNumber;
        var email = req.body.email;
        var password = req.body.password;

        var firstName = req.body.firstName;
        var middleName = req.body.middleName;
        var lastName = req.body.lastName;
        var bio = req.body.bio;
        var address = req.body.address;
        var gender = req.body.gender;
        var dateOfBirth = req.body.dateOfBirth;
        var latitude = req.body.latitude;
        var longitude = req.body.longitude;

        if(phoneNumber)     {data.local.phoneNumber = phoneNumber;}
        if(email)           {data.local.email = email;}
        if(password)        {data.local.password = password;}

        if(firstName)       {data.profile.firstName = firstName;}
        if(middleName)      {data.profile.middleName = middleName;}
        if(lastName)        {data.profile.lastName = lastName;}
        if(bio)             {data.profile.bio = bio;}
        if(address)         {data.profile.address = address;}
        if(gender)          {data.profile.gender = gender;}
        if(dateOfBirth)     {data.profile.dateOfBirth = dateOfBirth;}
        if(latitude)        {data.profile.posGeo.latitude = latitude;}
        if(longitude)       {data.profile.posGeo.longitude = longitude;}

        User.create(data, function (err, user) {
            if (err) {
                return next(new mongooseError(err));
            }
            // Sign Token
            var token = jwt.sign({ userId: user._id }, config.secret, {
                expiresIn: config.tokenExpiration
            });
            user.core.refreshTokens.push(token);
            user.save();
        });
    }

};
module.exports = controller;