var env = require(__base+'/app/env');
var restify = require('restify');

module.exports = function (server, middlewares, controllers) {

  // Tools
  if (env.stage == 'development') {
    server.get({ path: '/app-token', version: '1.0.0' }, controllers['tool'].getAppToken);
  }

  // User public service
  server.post({ path: '/user/register', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postRegister);
/*
  server.post({ path: '/user/authenticate', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postAuthenticate);
  server.post({ path: '/user/refresh-token', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postRefreshToken);
  server.post({ path: '/user/forgot', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postForgot);
  server.post({ path: '/user/forgot/resend-token', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postResendToken);
  server.post({ path: '/user/forgot/check', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postCheck);
  server.post({ path: '/user/forgot/reset', version: '1.0.0' }, middlewares.user, controllers['v1-user-core'].postReset);

  // User Auth
  server.post({ path: '/user/logout', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postLogout);
  server.post({ path: '/user/logout-all', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postLogoutAll);
  server.post({ path: '/user/verify', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postVerify);
  server.post({ path: '/user/resend-code', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postResendCode);
  server.post({ path: '/user/device-token', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postDeviceToken);
  server.del ({ path: '/user/device-token/:token', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].deleteDeviceToken);
  server.post({ path: '/user/password', version: '1.0.0' }, middlewares['user-auth'], controllers['v1-user-core'].postUpdatePassword);
*/

};
