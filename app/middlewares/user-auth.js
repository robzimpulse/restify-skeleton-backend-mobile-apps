var restify = require('restify');
var jwt = require('jsonwebtoken');

var config = require(__base + '/app/config');
var User = require(__base+'/app/models/user');

var middleware = function foo(req, res, next) {

  var token = req.headers['x-access-token'];

	if (req.service != 'user' || !token) {
		return next(new restify.UnauthorizedError(i18n.__('Akses ditolak')));
	}

  jwt.verify(token, config.secret, function (err, decoded) {
    if (err) {
      return next(new restify.UnauthorizedError(err.message));
    }

    User.findOne({
      _id: decoded.userId,
      logoutTime: { $lt: new Date(decoded.iat * 1000) }
    }, '', function (err, user) {
      if (err) {
        return next(new restify.InternalServerError(500));
      }

      if (user == null) {
        return next(new restify.ForbiddenError(i18n.__('Akses ditolak')));
      }

      req.decoded = decoded;
      return next();
    });
  });
};

module.exports = middleware;