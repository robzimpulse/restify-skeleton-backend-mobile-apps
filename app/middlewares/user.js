var restify = require('restify');

var middleware = function foo(req, res, next) {

  if (req.service != 'user') {
    return next(new restify.ForbiddenError(i18n.__('Akses ditolak')));
  }

  return next();

};

module.exports = middleware;