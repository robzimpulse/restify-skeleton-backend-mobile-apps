var restify = require('restify');
var jwt = require('jsonwebtoken');
var config = require(__base + '/app/config');

var middleware = function foo(req, res, next) {
  var appToken = req.headers['x-app-token'];

	if (!appToken) {
		return next(new restify.UnauthorizedError('Unauthorized'));
	}

  jwt.verify(appToken, config.secret, { ignoreExpiration: true }, function (err, decoded) {
    if (err) {
      return next(new restify.UnauthorizedError('Unauthorized'));
    }

    if (decoded.apiToken == config.userApiToken) {
      req.service = 'user';
    }

    else if (decoded.apiToken == config.adminApiToken) {
      req.service = 'admin';
    }

    else {
      return next(new restify.ForbiddenError('Forbidden'));
    }

    return next();

  });
};

module.exports = middleware;