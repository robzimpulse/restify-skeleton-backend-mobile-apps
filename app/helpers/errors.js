var ruhroh = require("ruhroh");

var ERRORS = [
    ['ValidationFailed', 422, 'Validation of input parameters failed. See "errors" array for specific failures.'],
];

module.exports = ruhroh.createErrorClasses(ERRORS);