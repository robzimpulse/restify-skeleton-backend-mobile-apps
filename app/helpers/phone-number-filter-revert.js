module.exports = function (phoneNumber) {
    if (!phoneNumber) {
        return phoneNumber;
    }

    if (phoneNumber.substr(0,3) == '+62') {
        phoneNumber = phoneNumber.replace('+62','0');
    }

    return phoneNumber;
}