module.exports = function (phoneNumber) {
	if (!phoneNumber) {
		return phoneNumber;
	}

	if (phoneNumber.charAt(0) == 0) {
		while(phoneNumber.charAt(0) === '0') {
    	phoneNumber = phoneNumber.substr(1);
		}
	} else {
		phoneNumber = phoneNumber.replace('+62', '');
	}

	phoneNumber = '+62'+phoneNumber;

	return phoneNumber;
}