var restify = require('restify');
var util = require('util');

function Error(err) {
	if (err.name != 'ValidationError') {
		return restify.InternalServerError("500");
	}

	var errors = [];

	for (var field in err.errors) {
		errors.push({
			message: err.errors[field].message,
			path: err.errors[field].path,
			value: err.errors[field].value
		});
	}

  restify.RestError.call(this, {
    restCode: 'UnprocessableEntityError',
    statusCode: 422,
    message: err.message,
    constructorOpt: Error
  });
  this.name = 'Error';
  this.body.errors = errors;
};

util.inherits(Error, restify.RestError);

module.exports = Error;