var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var config = require(__base+'/app/config');
var Schema = mongoose.Schema;
var captainHook  = require('captain-hook');

require(__base+'/app/helpers/mongoose-paginate');

var UserSchema = new Schema({
  local: {
    phoneNumber: {
      type: String,
      unique: true,
      sparse: true
    },
    email: {
      type: String,
      unique: true,
      required: true
    },
    password: {
      type: String,
      select: false
    }
  },
  profile:{
    firstName: String,
    middleName: String,
    lastName: String,
    image: String,
    bio: String,
    address: String,
    gender: {
      type: String,
      enum: ['male', 'female']
    },
    posGeo: {
      latitude: Number,
      longitude: Number,
      time: Date
    },
    dateOfBirth: {
      type: Date
    }
  },
  core: {
    createdAt: {
      type: Date,
      default: Date.now
    },
    verified: {
      type: Boolean,
      default: false
    },
    verificationCode: {
      type: Number,
      default: generateVerificationCode(),
      select: false
    },
    deviceTokens: {
      type: [String],
      select: false
    },
    refreshTokens: {
      type: [String],
      select: false
    },
    logoutTime: {
      type : Date,
      default: Date.now() - 3600000,
      select: false
    },
    resetPasswordToken: {
      token: String,
      expiration: Date
    }
  }
});

UserSchema.plugin(captainHook);

UserSchema.set('toJSON', {
  virtuals: true
});

UserSchema.set('toObject', {
  virtuals: true
});

UserSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) {return next();}
  bcrypt.genSalt(config.saltFactor, function(err, salt) {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

UserSchema.preCreate(function(data, next){
  console.log(data);
  next();
});

UserSchema.postCreate(function(data, next){
  console.log(data);
  next();
});

UserSchema.preUpdate(function(data, next){
  console.log(data);
  next();
});

UserSchema.postUpdate(function(data, next){
  console.log(data);
  next();
});

UserSchema.virtual('profile.displayName').get(function () {

  var user = this;
  var name = '';
  if(user.profile.firstName){
    name += capitalise(user.profile.firstName);
  }
  if(user.profile.middleName){
    name += capitalise(user.profile.middleName);
  }
  if(user.profile.lastName){
    name += capitalise(user.profile.lastName);
  }

  return name;
});

UserSchema.methods = {

  sendSMSVerificationCode: function (cb) {
    if (this.core.verificationCode) {
      console.log(config.notificationMessage.verification+this.core.verificationCode);
      cb(null,true);      // signature (err, successFlag)
      //cb(null,false);   // signature (err, successFlag)
    }
  },

  sendSMSResetPasswordToken: function (cb) {
    if (typeof this.core.resetPasswordToken === 'undefined') {
      return;
    }

    if (new Date(this.core.resetPasswordToken.expiration) < Date.now()) {
      return;
    }

    console.log(config.notificationMessage.resetPassword+this.core.resetPasswordToken.token);
    cb(null,true);      // signature (err, successFlag)
    //cb(null,false);   // signature (err, successFlag)
  },

  verify: function (verificationCode, cb) {
    if (this.core.verificationCode != verificationCode) {
      return cb('Invalid verification code');
    }

    this.core.verificationCode = '';
    this.core.verified = true;
    this.save(cb);
  }

};

function capitalise(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function generateVerificationCode(){
  var high = 9999;
  var low = 1001;
  return Math.floor(Math.random() * (high - low + 1) + low);
}

module.exports = mongoose.model('User', UserSchema);