var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    minifyCSS = require('gulp-minify-css'),
    cleanCSS = require('gulp-clean-css');

var onError = function( err ) {
  console.log( 'An error occurred:', err.message );
  this.emit( 'end' );
};

gulp.task('angular-admin-javascripts', function () {
  return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-route/angular-route.min.js',
        'angular/admin/**/*.js'
      ])
      .pipe(concat('all.min.js'))
      .pipe(uglify({
        mangle: false,
        compress: false,
        output: {
          ascii_only: true
        }
      }))
      .pipe(gulp.dest('public/admin/javascripts'));
});

gulp.task('angular-admin-index', function (){
  return gulp.src(['angular/admin/index.jade'])
      .pipe(jade())
      .pipe(gulp.dest('public/admin'));
});

gulp.task('angular-admin-templates', function (){
  return gulp.src(['angular/admin/views/**/*.jade'])
      .pipe(jade())
      .pipe(gulp.dest('public/admin/views'));
});

gulp.task('angular-admin-sass', function (){
  return gulp.src([
        'angular/admin/**/*.css'
        //'angular/admin/app.sass'
      ])
      //.pipe(sass({
      //  includePaths: require('node-normalize-scss').includePaths
      //}))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(concat('all.min.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('public/admin/stylesheets'));
});

gulp.task('watch', [
  'angular-admin-javascripts', 'angular-admin-index', 'angular-admin-templates', 'angular-admin-sass'
], function () {
  gulp.watch('angular/admin/**/*.js', ['angular-admin-javascripts']);
  gulp.watch('angular/admin/index.jade', ['angular-admin-index']);
  gulp.watch('angular/admin/**/*.jade', ['angular-admin-templates']);
  gulp.watch('angular/admin/**/*.css', ['angular-admin-sass']);
});

gulp.task('default', [
  'angular-admin-javascripts', 'angular-admin-index', 'angular-admin-templates', 'angular-admin-sass',
  'watch'
]);