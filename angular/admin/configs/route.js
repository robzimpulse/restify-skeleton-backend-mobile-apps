adminApp.config(['$routeProvider', function ($routeProvider) {
  
  // Routes
  $routeProvider
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginController'
    })
    .otherwise({
      redirectTo: '/login'
    });
}]);