adminApp.controller('MainController', function($scope, $auth, $location, $http, localStorageService) {


  /* Manage Logged In User */
  $scope.currentUser = null;

  $scope.setCurrentUser = function (admin, token) {
    localStorageService.set('currentUser', admin);
    localStorageService.set('token', token);

    var currentUser = admin;

    if (token) {
      currentUser.token = token;
    }

    $scope.currentUser = currentUser;
  };

  // Check currentUser from localStorage
  var currentUser = localStorageService.get('currentUser');
  var token = localStorageService.get('token');

  if (currentUser && token) {
    $scope.setCurrentUser(currentUser, token);
  }

  // Logout
  $scope.logout = function () {
    $auth.logout();
    localStorageService.remove('token');
    localStorageService.remove('currentUser');
    $location.path('/login');
  };

  // Unread Count
  $scope.unreadCount = 0;

  $scope.substractUnreadCount = function () {
    if ($scope.unreadCount > 0) {
      $scope.unreadCount--;
    }
  };

  var fetchUnreadTrxCount = function () {
    if (!$scope.currentUser) {
      return;
    }

    $http({
      method: "GET",
      url: base_url+"/transactions/unread/count",
      headers: {
        'x-access-token': $scope.currentUser.token
      }
    }).success(function(result, status, headers, config) {

      $scope.unreadCount = result.count;

    });
  };

  var fetchUnreadTrxCountTimer;

  $scope.fetchUnreadTrxCountStart = function () {
    fetchUnreadTrxCount();
    fetchUnreadTrxCountTimer = setInterval(fetchUnreadTrxCount, 30000);
  };

  $scope.fetchUnreadTrxCountStop = function () {
    clearInterval(fetchUnreadTrxCountTimer);
  };

  if ($scope.currentUser) {
    $scope.fetchUnreadTrxCountStart();
  }
  
});