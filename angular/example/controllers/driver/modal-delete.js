adminApp.controller('Driver.ModalDeleteController', function($scope, driver, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.driver = driver;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      password: $scope.password
    };

    $http({
      url: "/ladyjek/admin-api/driver/"+$scope.driver._id+"/delete",
      method: "POST",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.driver.name+' has successfully deleted.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.form['password'].$setValidity('server', false);
      serverErrors['password'] = data.message;

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});