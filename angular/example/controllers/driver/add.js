adminApp.controller('Driver.AddController', function($scope, $http, $location, cfpLoadingBar) {

  $scope.processing = false;
  $scope.driver = {};
  $scope.serverErrors = {};

  var getCurrentYmd = function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    }

    return yyyy+mm+dd;
  };

  $scope.save = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = $scope.driver;

    if (data.phoneNumber.charAt(0) == '0') {
      data.phoneNumber = '+62'+data.phoneNumber.substring(1);
    }

    data.idLadyJek = 'LJ' + getCurrentYmd() + '00000';
    data.emailRegistrant = $scope.currentUser.username;

    $http({
      url: "/ladyjek/admin-api/driver",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/drivers');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (message.errors) {
        for (field in message.errors) {
          $scope.form[field].$setValidity('server', false);
          serverErrors[field] = message.errors[field].message;
        }
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/drivers.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  }
  
});