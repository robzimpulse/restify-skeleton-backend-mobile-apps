adminApp.controller('Driver.ViewController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.driver = sharedProperties.get('driver');
  
  if (!$scope.driver || $scope.driver._id != $routeParams.id) {
    $location.path('/drivers');
  }
  
});