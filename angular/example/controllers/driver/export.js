adminApp.controller('Driver.ExportController', function($scope, $http) {

  $scope.export = function () {

    var data = {};

    if ($scope.from){
        data.from = $scope.from;
    }

    if ($scope.to){
        data.to = $scope.to;
    }

    $http({
      url: '/ladyjek/admin-api/dataDrivers/export',
      method: 'POST',
      responseType: 'arraybuffer',
      headers: {
      'x-access-token': $scope.currentUser.token
    },
        data: data
    }).success(function (data, status, headers, config) {

      var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
      var objectUrl = URL.createObjectURL(blob);
      window.open(objectUrl);

    }).error(function (data, status, headers, config) {

      Flash.create('danger', 'Something Wrong. Please contact web administrator.');

    });

  }

});