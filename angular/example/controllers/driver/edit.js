adminApp.controller('Driver.EditController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.processing = false;
  $scope.driver = sharedProperties.get('driver');
  
  if ($scope.driver.photo) {
    $scope.driver.getPhoto.url += '?' + new Date().getTime();
  }

  if (!$scope.driver || $scope.driver._id != $routeParams.id) {
    $location.path('/drivers');
  }

  $scope.driver.phoneNumber = $scope.driver.phoneNumber.replace('+62', '0');
  $scope.updateDriverName = $scope.driver.name;
  $scope.serverErrors = {};

  $scope.changePhoto = function () {
    sharedProperties.set('driver', $scope.driver);
    $location.path('/driver/'+$scope.driver._id+'/change-photo').search({});
  };

  $scope.deletePhoto = function () {
    $scope.processing = true;

    $http({
      url: "/ladyjek/admin-api/driver/"+$scope.driver._id+"/delete-photo",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      }
    }).success(function(data, status, headers, config) {

      $scope.processing = false;

      delete $scope.driver.photo;
      delete $scope.driver.getPhoto;

      Flash.create('success', 'Delete photo success.');

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

      Flash.create('danger', 'Something wrong. Please contact web administrator.');

    });
  };

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var driverProperties = sharedProperties.get('driver');
    var data = $.extend(driverProperties, $scope.driver);

    if (data.phoneNumber.charAt(0) == '0') {
      data.phoneNumber = '+62'+data.phoneNumber.substring(1);
    }

    $http({
      url: "/ladyjek/admin-api/driver/"+data._id+"/update",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/drivers');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (message.errors) {
        for (field in message.errors) {
          $scope.form[field].$setValidity('server', false);
          serverErrors[field] = message.errors[field].message;
        }
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/drivers.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});