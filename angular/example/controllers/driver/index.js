adminApp.controller('Driver.IndexController', function($scope, $http, $location, sharedProperties, ModalService) {

  var params = $location.search();

  var args = {
    sort: '-createdAt'
  };

  if (params.sort) {
    args.sort
      = $scope.sort
      = params.sort;

    $scope.sortOrder = params.sortOrder;
    if (params.sortOrder == 'desc') {
      args.sort = '-'+args.sort.replace('-', '');
    }
  }

  $scope.filterData = {};

  if (params.idLadyJek) {
    args.idLadyJek
      = $scope.filterData.idLadyJek
      = params.idLadyJek;
  }

  if (params.name) {
    args.name
      = $scope.filterData.name
      = params.name;
  }

  if (params.phoneNumber) {
    args.phoneNumber
      = $scope.filterData.phoneNumber
      = params.phoneNumber;

    if (args.phoneNumber.charAt(0) == '0') {
      args.phoneNumber = '+62'+args.phoneNumber.substring(1);
    }
  }

  if (params.emailRegistrant) {
    args.emailRegistrant
      = $scope.filterData.emailRegistrant
      = params.emailRegistrant;
  }

  // Paginate param
  var page = 1;
  if (params.page > 1) {
    page = params.page;
  }

  $http({
    method: "GET",
    url: "/ladyjek/admin-api/drivers/page/"+page,
    params: args,
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    if (result.error) {
      Flash.create('danger', result.message);
    }

    var data = result.data;

    $scope.page = page;
    $scope.pageSize = data.limit;
    $scope.total = data.total;
    $scope.totalPage = Math.ceil(data.total/data.limit);
    $scope.drivers = data.drivers;

  });

  $scope.changeFilter = function () {
    delete params.page;
    delete params.idLadyJek;
    delete params.name;
    delete params.phoneNumber;
    delete params.emailRegistrant;

    if ($scope.filterData.idLadyJek) {
      params.idLadyJek = $scope.filterData.idLadyJek;
    }

    if ($scope.filterData.name) {
      params.name = $scope.filterData.name;
    }

    if ($scope.filterData.phoneNumber) {
      params.phoneNumber = $scope.filterData.phoneNumber;
    }

    if ($scope.filterData.emailRegistrant) {
      params.emailRegistrant = $scope.filterData.emailRegistrant;
    }

    $location.search(params);
  };

  $scope.resetFilter = function () {
    delete params.page;
    delete params.idLadyJek;
    delete params.name;
    delete params.phoneNumber;
    delete params.emailRegistrant;

    $location.search(params);
  };

  $scope.changeSort = function (sort) {
    delete params.page;
    
    if (params.sort != sort) {
      params.sort = sort;
      params.sortOrder = 'asc';
    } else {
      if (params.sortOrder && params.sortOrder == 'desc') {
        params.sortOrder = 'asc';
      } else {
        params.sortOrder = 'desc';
      }
    }

    $location.search(params);
  };

  $scope.changePage = function (text, page, pageSize, total) {
    params.page = page;
    $location.search(params);
  };

  // Action
  $scope.action = function (action, index) {
    var driver = $scope.drivers[index];

    switch (action) {
      case 'view':
        sharedProperties.set('driver', driver);
        $location.path('/driver/'+driver._id+'/view').search({});
        break;
      case 'reset-password':
        
        ModalService.showModal({
          templateUrl: 'views/driver/modal-reset-password.html',
          controller: 'Driver.ModalResetPasswordController',
          inputs: { driver: driver }
        });

        break;
      case 'edit':
        sharedProperties.set('driver', driver);
        $location.path('/driver/'+driver._id+'/edit').search({});
        break;
      case 'activate':
        sharedProperties.set('driver', driver);
        $location.path('/driver/'+driver._id+'/activate').search({});
        break;
      case 'delete':
        
        ModalService.showModal({
          templateUrl: 'views/driver/modal-delete.html',
          controller: 'Driver.ModalDeleteController',
          inputs: { driver: driver }
        });

        break;
    }
  };
  
});