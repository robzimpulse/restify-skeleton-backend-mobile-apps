adminApp.controller('Driver.ModalResetPasswordController', function($scope, driver, close, $http, $route, localStorageService, Flash, ladyjekService) {

  $scope.processing = false;
  $scope.driver = driver;
  $scope.password = ladyjekService.generatePassdigit();

  $scope.resetPassword = function () {

    $scope.processing = true;
    
    var data = {
      id: $scope.driver._id,
      password: $scope.password
    };

    $http({
      url: "/ladyjek/admin-api/driver/"+$scope.driver._id+"/reset-password",
      method: "POST",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', 'Passdigits '+$scope.driver.name+' has successfully reset.');
      }, 100);

    }).error(function(data, status, headers, config) {

      $scope.processing = false;
      Flash.create('danger', 'Something Wrong. Please contact web administrator.');

    });

  };

  $scope.close = close;
  
});