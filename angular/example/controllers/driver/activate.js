adminApp.controller('Driver.ActivateController', function($scope, $routeParams, $http, $location, sharedProperties, ladyjekService, Flash) {

  $scope.driver = sharedProperties.get('driver');
  
  if (!$scope.driver || $scope.driver._id != $routeParams.id) {
    $location.path('/drivers');
  }

  $scope.data = {
    id: $scope.driver._id,
    password: ladyjekService.generatePassdigit(),
    kelengkapanDocument: []
  };

  ladyjekService.getNewLadyJekId(function (err, result) {
    $scope.data.idSet = result.idSet;
    $scope.data.idLadyJek = result.idLadyJek;
  });

  $scope.processing = false;
  $scope.kelengkapanDocumentError = false;

  $scope.activate = function () {

    $scope.processing = true;

    var input = $scope.data;
    if ( input.kelengkapanDocumentLainnya ) {
      input.kelengkapanDocument.push(input.kelengkapanDocumentLainnya);
    }
    input.kelengkapanDocument = input.kelengkapanDocument.join(', ');

    ladyjekService.getNewLadyJekId(function (err, result) {
      
      $scope.data.idSet = result.idSet;
      $scope.data.idLadyJek = result.idLadyJek;

      $http({
        url: "/ladyjek/admin-api/driver/"+input.id+"/activate",
        method: "POST",
        headers: {
          'x-access-token': $scope.currentUser.token
        },
        data: input
      }).success(function(data, status, headers, config) {
        
        $location.path('/drivers');
        setTimeout(function () {
          Flash.create('success', $scope.driver.name+' has successfully activated.');
        }, 100);

      }).error(function(data, status, headers, config){

        $scope.processing = false;
        Flash.create('danger', 'Something wrong. Please contact web administrator.');

      });
    });

  }
  
});