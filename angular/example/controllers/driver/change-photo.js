adminApp.controller('Driver.ChangePhotoController', function($scope, $routeParams, $http, $location, sharedProperties, Upload, $timeout, Flash) {

  $scope.processing = false;
  $scope.driver = sharedProperties.get('driver');
  
  if (!$scope.driver || $scope.driver._id != $routeParams.id) {
    $location.path('/drivers');
  }

  $scope.cropper = {};
  $scope.cropper.sourceImage = null;
  $scope.cropper.croppedImage = null;

  var dataURItoBlob = function(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    var array = [];
    for(var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: mimeString});
  };

  $scope.upload = function (dataUrl) {
    $scope.processing = true;
    $scope.result = 0;

    Upload.upload({
      url: "/ladyjek/admin-api/driver/"+$scope.driver._id+"/change-photo",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: {
        file: dataURItoBlob(dataUrl)
      }
    }).then(function (response) {
      $scope.processing = false;

      $timeout(function () {
        var data = response.data.data;
        $scope.driver.photo = data.fileName;
        $scope.driver.getPhoto = data;

        sharedProperties.set('driver', $scope.driver);

        $location.path('driver/'+$scope.driver._id+'/edit');
        Flash.create('success', 'Change photo success');
      });
    }, function (response) {
      $scope.processing = false;

      if (response.status > 0) {
        Flash.create('danger', 'Something wrong: '+response.status + ': ' + response.data+'. Please contact web administrator.');
      }
    }, function (evt) {
      $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
    });
  };

});