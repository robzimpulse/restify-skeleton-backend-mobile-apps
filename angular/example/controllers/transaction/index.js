adminApp.controller('Transaction.IndexController', function($scope, $http, $location, sharedProperties, ModalService) {

	console.log($scope.unreadCount);

	var params = $location.search();

	var args = {
		sort: '-createdAt'
		//sort: '-status'
	};

	if (params.sort) {
		args.sort
			= $scope.sort
			= params.sort;

		$scope.sortOrder = params.sortOrder;
		if (params.sortOrder == 'desc') {
			args.sort = '-'+args.sort.replace('-', '');
		}
	}

	$scope.filterData = {};

	if (params.prettyId) {
		args.prettyId
			= $scope.filterData.prettyId
			= params.prettyId;
	}

	if (params.user) {
		var paramsUser = JSON.parse(params.user);
		args.user
			= $scope.filterData.user
			= paramsUser;
	}

	if (params.hawker) {
		var paramsHawker = JSON.parse(params.hawker);
		args.hawker
			= $scope.filterData.hawker
			= paramsHawker;
	}

	if (params.menu) {
		var paramsMenu = JSON.parse(params.menu);
		args.menu
			= $scope.filterData.menu
			= paramsMenu;
	}

	if (params.type) {
		args.type
			= $scope.filterData.type
			= params.type;
	}

	if (params.price) {
		args.price
			= $scope.filterData.price
			= params.price;
	}

	if (params.address) {
		args.address
			= $scope.filterData.address
			= params.address;
	}

	if (params.openAt) {
		var date = new Date(params.openAt);
		args.openAt
			= $scope.filterData.openAt
			= date.toISOString();
	}

	if (params.status) {
		args.status
			= $scope.filterData.status
			= params.status;
	}

	if (params.unread) {
		args.unread
			= $scope.filterData.unread
			= params.unread;
	}

	if (params.refundNeeded) {
		args.refundNeeded
			= $scope.filterData.refundNeeded
			= params.refundNeeded;
	}

	// Paginate param
	var page = 1;
	if (params.page > 1) {
		page = params.page;
	}

	$http({
		method: "GET",
		url: base_url+"/transactions/"+page,
		params: args,
		headers: {
			'x-access-token': $scope.currentUser.token
		}
	}).success(function(result, status, headers, config) {

		var data = result;

		$scope.page = page;
		$scope.pageSize = data.limit;
		$scope.total = data.total;
		$scope.totalPage = Math.ceil(data.total/data.limit);
		$scope.transactions = data.transactions;

	});



	$scope.filterUsers = [
		$scope.filterData.user
	];
	$scope.filterHawkers = [
		$scope.filterData.hawker
	];
	$scope.filterMenus = [
		$scope.filterData.menu
	];

	$scope.refreshFilterUsers = function (user) {
		if (user.length < 3) {
			return;
		}

		return $http({
			url: base_url+"/users/list",
			method: "GET",
			headers: {
				'x-access-token': $scope.currentUser.token
			},
			params: { name: user }
		}).success(function(data, status, headers, config) {

			$scope.filterUsers = data;

		});
	};

	$scope.refreshFilterHawkers = function (hawker) {
		if (hawker.length < 3) {
			return;
		}

		$http({
			url: base_url+"/hawkers/list",
			method: "GET",
			headers: {
				'x-access-token': $scope.currentUser.token
			},
			params: { name: hawker }
		}).success(function(data, status, headers, config) {

			$scope.filterHawkers = data;
		});
	};

	$scope.refreshFilterMenus = function (menu) {
		if (menu.length < 3) {
			return;
		}

		$http({
			url: base_url+"/menus/list",
			method: "GET",
			headers: {
				'x-access-token': $scope.currentUser.token
			},
			params: { name: menu }
		}).success(function(data, status, headers, config) {

			$scope.filterMenus = data;
		});
	};

	$scope.changeFilter = function () {
		delete params.page;
		delete params.prettyId;
		delete params.user;
		delete params.type;
		delete params.price;
		delete params.address;
		delete params.status;
		delete params.unread;
		delete params.refundNeeded;
		delete params.openAt;

		if ($scope.filterData.prettyId) {
			params.prettyId = $scope.filterData.prettyId;
		}

		if ($scope.filterData.user) {
			params.user = JSON.stringify($scope.filterData.user);
		}

		if ($scope.filterData.hawker) {
			params.hawker = JSON.stringify($scope.filterData.hawker);
		}

		if ($scope.filterData.menu) {
			params.menu = JSON.stringify($scope.filterData.menu);
		}

		if ($scope.filterData.type) {
			params.type = $scope.filterData.type;
		}

		if ($scope.filterData.price) {
			params.price = $scope.filterData.price;
		}

		if ($scope.filterData.address) {
			params.address = $scope.filterData.address;
		}

		if ($scope.filterData.status) {
			params.status = $scope.filterData.status;
		}

		if ($scope.filterData.unread) {
			params.unread = $scope.filterData.unread;
		}

		if ($scope.filterData.refundNeeded) {
			params.refundNeeded = $scope.filterData.refundNeeded;
		}

		if ($scope.filterData.openAt) {
			var data = $scope.filterData.openAt.split('/');
			var date = new Date(data[2],data[1]-1,data[0]);
			date.setHours(12,0,0,0);
			params.openAt = date;
		}

		$location.search(params);
	};

	$scope.resetFilter = function () {
		delete params.user;
		delete params.type;
		delete params.price;
		delete params.address;
		delete params.note;
		delete params.status;
		delete params.unread;
		delete params.refundNeeded;
		delete params.hawker;
		delete params.user;
		delete params.menu;
		delete params.openAt;
		$location.search(params);
	};

	$scope.changeSort = function (sort) {
		delete params.page;
		console.log(params);

		if (params.sort != sort) {
			params.sort = sort;
			params.sortOrder = 'asc';
		} else {
			if (params.sortOrder && params.sortOrder == 'desc') {
				params.sortOrder = 'asc';
			} else {
				params.sortOrder = 'desc';
			}
		}

		$location.search(params);
	};

	$scope.changePage = function (text, page, pageSize, total) {
		params.page = page;
		$location.search(params);
	};

	// Action
	$scope.action = function (action, index) {
		var transaction = $scope.transactions[index];

		switch (action) {
			case 'detail':
				sharedProperties.set('transaction', transaction);
				$location.path('/transaction/'+transaction._id+'/detail').search({});
				break;
			case 'return':
				ModalService.showModal({
					templateUrl: 'views/transaction/modal-return.html',
					controller: 'Transaction.ModalReturnController',
					inputs: { transaction: transaction }
				});
				break;
			case 'edit':
				ModalService.showModal({
					templateUrl: 'views/transaction/modal-update.html',
					controller: 'Transaction.ModalUpdateController',
					inputs: { transaction: transaction, substractUnreadCount: $scope.substractUnreadCount }
				});
				break;
			case 'delete':
				ModalService.showModal({
					templateUrl: 'views/transaction/modal-delete.html',
					controller: 'Transaction.ModalDeleteController',
					inputs: { transaction: transaction }
				});

				break;
		}
	};

});