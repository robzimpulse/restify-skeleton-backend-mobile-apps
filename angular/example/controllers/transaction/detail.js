adminApp.controller('Transaction.DetailController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.transaction = sharedProperties.get('transaction');
  
  if (!$scope.transaction || $scope.transaction._id != $routeParams.id) {
    $location.path('/transaction');
  }

  $http({
    method: "PUT",
    url: base_url+"/transaction/"+$scope.transaction._id+"/read",
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    $scope.substractUnreadCount();

  });
  
});