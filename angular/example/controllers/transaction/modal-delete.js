adminApp.controller('Transaction.ModalDeleteController', function($scope, transaction, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.transaction = transaction;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      _id: $scope.transaction._id,
      status : 'canceled'
    };

    $http({
      url: base_url + "/transaction/"+$scope.transaction._id,
      method: "PUT",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.transaction.prettyId+' has successfully canceled.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});