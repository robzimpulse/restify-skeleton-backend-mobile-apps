adminApp.controller('Transaction.ModalReturnController', function($scope, transaction, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.transaction = transaction;
  $scope.serverErrors = {};

  $scope.confirm = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    
    $http({
      url: base_url + "/transaction/"+$scope.transaction._id+"/return",
      method: "PUT",
      headers: {
        'x-access-token': localStorageService.get('token')
      }
    }).success(function (data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.transaction.prettyId+' has successfully returned.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});