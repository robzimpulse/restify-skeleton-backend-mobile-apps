adminApp.controller('Transaction.ModalUpdateController', function($scope, transaction, substractUnreadCount, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.transaction = transaction;
  $scope.serverErrors = {};

  $scope.process = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      _id: $scope.transaction._id,
      status : 'processing'
    };

    $http({
      url: base_url + "/transaction/"+$scope.transaction._id,
      method: "PUT",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      $http({
        method: "PUT",
        url: base_url+"/transaction/"+$scope.transaction._id+"/read",
        headers: {
          'x-access-token': localStorageService.get('token')
        }
      }).success(function(result, status, headers, config) {

        substractUnreadCount();

      });

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.transaction.prettyId+' has successfully updated.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});