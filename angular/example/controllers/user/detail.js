adminApp.controller('User.DetailController', function($scope, $routeParams, $http, $location, localStorageService, sharedProperties) {

  $scope.user = sharedProperties.get('user');
  
  if (!$scope.user || $scope.user._id != $routeParams.id) {
    $location.path('/users');
  }

  $scope.verify = function () {

    $scope.processing = true;

    var serverErrors = {};
    var data = {};

    $http({
      url: base_url + "/user/"+$scope.user._id+"/trusted",
      method: "POST",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      console.log('success');
      $location.path('/users');

    }).error(function(data, status, headers, config) {

      $scope.processing = false;

    });

  };
});