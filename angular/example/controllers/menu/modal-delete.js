adminApp.controller('Menu.ModalDeleteController', function($scope, menu, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.menu = menu;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;

    var serverErrors = {};

    $http({
      url: base_url+"/menu/"+$scope.menu._id,
      method: "DELETE",
      headers: {
        'x-access-token': localStorageService.get('token')
      }
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.menu.name+' has successfully deleted.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.form['password'].$setValidity('server', false);
      serverErrors['password'] = data.message;

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});