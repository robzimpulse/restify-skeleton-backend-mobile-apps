adminApp.controller('Menu.EditController', function($scope, $routeParams, $http, $location, sharedProperties, Flash) {

  $scope.processing = true;

  var menu = sharedProperties.get('menu');

  var selectedHawker = menu.hawker;
  if (menu.hawker._id) {
    selectedHawker = menu.hawker._id;  
  }

  var selectedParent = '';
  if (menu.parent) {
    selectedParent = menu.parent;
    if (menu.parent._id) {
      selectedParent = menu.parent._id;
    }
  }
  
  delete menu.hawker;
  delete menu.parent;
  $scope.menu = menu;

  if (!menu || menu._id != $routeParams.id) {
    $location.path('/menus');
  }

  if (menu.speed && menu.speed.option) {
    var objectOption = {};
    for (var i=0; i<menu.speed.option.length; i++) {
      objectOption[menu.speed.option[i]] = menu.speed.option[i];
    }
    menu.speed.option = objectOption;
  }

  if (menu.daysAvailable) {
    var daysAvailableOption = {};
    for (var i=0; i<menu.daysAvailable.length; i++) {
      daysAvailableOption[menu.daysAvailable[i]] = menu.daysAvailable[i];
    }
    menu.daysAvailable = daysAvailableOption;
  }

  $scope.updateMenuName = menu.name;
  $scope.serverErrors = {};
  $scope.parents = [];
  $scope.hawkers = [];

  $http({
    url: base_url+"/menus/parents",
    method: "GET",
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(data, status, headers, config) {

    $scope.processing = false;
    $scope.parents = data;

    setTimeout(function () {
      $scope.menu.parent = selectedParent;
    }, 1);

  });

  $http({
    url: base_url+"/hawkers/list",
    method: "GET",
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(data, status, headers, config) {

    $scope.processing = false;
    $scope.hawkers = data;

    setTimeout(function () {
      $scope.menu.hawker = selectedHawker;
    }, 1);

  });

  $scope.changePhoto = function () {
    sharedProperties.set('menu', $scope.menu);
    $location.path('/menu/'+$scope.menu._id+'/change-photo').search({});
  };

  $scope.deletePhoto = function () {
    $scope.processing = true;

    $http({
      url: base_url+"/menu/"+$scope.menu._id+"/photo",
      method: "DELETE",
      headers: {
        'x-access-token': $scope.currentUser.token
      }
    }).success(function(data, status, headers, config) {

      $scope.processing = false;

      delete $scope.menu.photo;
      delete $scope.menu.getPhoto;

      $location.path('/menus');

      Flash.create('success', 'Delete photo success.');

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

      Flash.create('danger', 'Something wrong. Please contact web administrator.');

    });
  };

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var menuProperties = sharedProperties.get('menu');
    var data = $.extend(menuProperties, $scope.menu);

    $http({
      url: base_url+"/menu/"+data._id,
      method: "PUT",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/menus');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/menus.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});