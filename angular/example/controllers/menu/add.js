adminApp.controller('Menu.AddController', function($scope, $http, $location, sharedProperties) {

  $scope.processing = false;
  $scope.menu = {};
  $scope.serverErrors = {};
  $scope.parents = [];
  $scope.hawkers = [];

  $http({
    url: base_url+"/menus/parents",
    method: "GET",
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(data, status, headers, config) {

    $scope.processing = false;
    $scope.parents = data;

  });

  $http({
    url: base_url+"/hawkers/list",
    method: "GET",
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(data, status, headers, config) {

    $scope.hawkers = data;

  });

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var data = $scope.menu;
    console.log(data);
    $http({
      url: base_url+"/menu",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      sharedProperties.set('menu', data);
      $location.path('/menu/'+data._id+'/change-photo');

    }).error(function(data, status, headers, config) {

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (data.message.code == 11000) {
        var getField = message.errmsg.match(/menus.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  }
  
});