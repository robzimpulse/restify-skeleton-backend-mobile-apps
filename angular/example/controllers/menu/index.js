adminApp.controller('Menu.IndexController', function($scope, $http, $location, sharedProperties, ModalService) {

  var params = $location.search();

  var args = {
    sort: '-createdAt'
  };

  if (params.sort) {
    args.sort
      = $scope.sort
      = params.sort;

    $scope.sortOrder = params.sortOrder;
    if (params.sortOrder == 'desc') {
      args.sort = '-'+args.sort.replace('-', '');
    }
  }

  $scope.filterData = {};

  if (params.prettyId) {
    args.prettyId
      = $scope.filterData.prettyId
      = params.prettyId;
  }

  if (params.name) {
    args.name
      = $scope.filterData.name
      = params.name;
  }

  if (params.type) {
    args.type
      = $scope.filterData.type
      = params.type;
  }

  if (params.price) {
    args.price
      = $scope.filterData.price
      = params.price;
  }

  if (params.openAt) {
    args.openAt
      = $scope.filterData.openAt
      = params.openAt;
  }

  // Paginate param
  var page = 1;
  if (params.page > 1) {
    page = params.page;
  }

  $http({
    method: "GET",
    url: base_url+"/menus/page/"+page,
    params: args,
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    var data = result;

    $scope.page = page;
    $scope.pageSize = data.limit;
    $scope.total = data.total;
    $scope.totalPage = Math.ceil(data.total/data.limit);
    $scope.menus = data.menus;

  });

  $scope.changeFilter = function () {
    delete params.page;
    delete params.prettyId;
    delete params.name;
    delete params.type;
    delete params.price;
    delete params.openAt;

    if ($scope.filterData.prettyId) {
      params.prettyId = $scope.filterData.prettyId;
    }

    if ($scope.filterData.name) {
      params.name = $scope.filterData.name;
    }

    if ($scope.filterData.type) {
      params.type = $scope.filterData.type;
    }

    if ($scope.filterData.price) {
      params.price = $scope.filterData.price;
    }

    if ($scope.filterData.openAt) {
      params.openAt = $scope.filterData.openAt;
    }

    $location.search(params);
  };

  $scope.resetFilter = function () {
    delete params.page;
    delete params.prettyId;
    delete params.name;
    delete params.type;
    delete params.price;
    delete params.openAt;

    $location.search(params);
  };

  $scope.changeSort = function (sort) {
    delete params.page;
    
    if (params.sort != sort) {
      params.sort = sort;
      params.sortOrder = 'asc';
    } else {
      if (params.sortOrder && params.sortOrder == 'desc') {
        params.sortOrder = 'asc';
      } else {
        params.sortOrder = 'desc';
      }
    }

    $location.search(params);
  };

  $scope.changePage = function (text, page, pageSize, total) {
    params.page = page;
    $location.search(params);
  };

  // Action
  $scope.action = function (action, index) {
    var menu = $scope.menus[index];

    switch (action) {
      case 'edit':
        sharedProperties.set('menu', menu);
        $location.path('/menu/'+menu._id+'/edit').search({});
        break;
      case 'delete':
        
        ModalService.showModal({
          templateUrl: 'views/menu/modal-delete.html',
          controller: 'Menu.ModalDeleteController',
          inputs: { menu: menu }
        });

        break;
    }
  };
  
});