adminApp.controller('Supply.EditController', function($scope, $routeParams, $http, $location, sharedProperties, Flash) {

  $scope.processing = false;

  var supply = sharedProperties.get('supply');

  $scope.supply = supply;

  if (!supply || supply._id != $routeParams.id) {
    $location.path('/supplies');
  }

  $scope.updateSupplyName = supply.name;
  $scope.serverErrors = {};

  $scope.changePhoto = function () {
    sharedProperties.set('supply', $scope.supply);
    $location.path('/supply/'+$scope.supply._id+'/change-photo').search({});
  };

  $scope.deletePhoto = function () {
    $scope.processing = true;

    $http({
      url: base_url+"/supply/"+$scope.supply._id+"/photo",
      method: "DELETE",
      headers: {
        'x-access-token': $scope.currentUser.token
      }
    }).success(function(data, status, headers, config) {

      $scope.processing = false;

      delete $scope.supply.photo;
      delete $scope.supply.getPhoto;

      $location.path('/supplies');

      Flash.create('success', 'Delete photo success.');

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

      Flash.create('danger', 'Something wrong. Please contact web administrator.');

    });
  };

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var supplyProperties = sharedProperties.get('supply');
    var data = $.extend(supplyProperties, $scope.supply);
    console.log(data);
    $http({
      url: base_url+"/supply/"+data._id,
      method: "PUT",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/supplies');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/supplies.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});