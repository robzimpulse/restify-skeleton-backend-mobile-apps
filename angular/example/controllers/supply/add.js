adminApp.controller('Supply.AddController', function($scope, $http, $location, sharedProperties) {

  $scope.processing = false;
  $scope.supply = {};
  $scope.serverErrors = {};

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var data = $scope.supply;
    console.log(data);
    $http({
      url: base_url+"/supply",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      sharedProperties.set('supply', data);
      $location.path('/supply/'+data._id+'/change-photo');

    }).error(function(data, status, headers, config) {

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (data.message.code == 11000) {
        var getField = message.errmsg.match(/supplies.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  }
  
});