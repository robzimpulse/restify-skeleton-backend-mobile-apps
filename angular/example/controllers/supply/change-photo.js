adminApp.controller('Supply.ChangePhotoController', function($scope, $routeParams, $http, $location, sharedProperties, Upload, $timeout, Flash) {

  $scope.processing = false;
  $scope.supply = sharedProperties.get('supply');
  
  if (!$scope.supply || $scope.supply._id != $routeParams.id) {
    $location.path('/supplies');
  }

  $scope.cropper = {};
  $scope.cropper.sourceImage = null;
  $scope.cropper.croppedImage = null;

  var dataURItoBlob = function(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    var array = [];
    for(var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: mimeString});
  };

  $scope.upload = function (dataUrl) {
    $scope.processing = true;
    $scope.result = 0;

    Upload.upload({
      url: base_url+"/supply/"+$scope.supply._id+"/photo",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: {
        file: dataURItoBlob(dataUrl)
      }
    }).then(function (response) {
      $scope.processing = false;

      $timeout(function () {
        var data = response.data;
        console.log($scope.supply);

        $scope.supply.images[0] = data.fileName;
        $scope.supply.imageUrls[0] = data.url;

        sharedProperties.set('supply', $scope.supply);

        $location.path('supply/'+$scope.supply._id+'/edit');
        Flash.create('success', 'Change image success');
      });

    }, function (response) {
      $scope.processing = false;

      if (response.status > 0) {
        Flash.create('danger', 'Something wrong: '+response.status + ': ' + response.data+'. Please contact web administrator.');
      }
    }, function (evt) {
      $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
    });
  };

});