adminApp.controller('Hawker.EditController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.processing = false;
  $scope.hawker = sharedProperties.get('hawker');
  
  if (!$scope.hawker || $scope.hawker._id != $routeParams.id) {
    $location.path('/hawkers');
  }

  $scope.updateHawkerName = $scope.hawker.name;
  $scope.serverErrors = {};

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var hawkerProperties = sharedProperties.get('hawker');
    var data = $.extend(hawkerProperties, $scope.hawker);

    $http({
      url: base_url+"/hawker/"+data._id,
      method: "PUT",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/hawkers');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/hawkers.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});