adminApp.controller('Hawker.ModalUpdateController', function($scope, hawker, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.hawker = hawker;
  $scope.serverErrors = {};

  $scope.process = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      _id: $scope.hawker._id,
      trusted : 'true'
    };

    $http({
      url: base_url + "/hawker/"+$scope.hawker._id+"/trusted",
      method: "POST",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.hawker._id+' has successfully updated.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});