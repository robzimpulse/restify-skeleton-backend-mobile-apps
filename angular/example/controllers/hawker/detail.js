adminApp.controller('Hawker.DetailController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.hawker = sharedProperties.get('hawker');
  
  if (!$scope.hawker || $scope.hawker._id != $routeParams.id) {
    $location.path('/hawkers');
  }
  
});