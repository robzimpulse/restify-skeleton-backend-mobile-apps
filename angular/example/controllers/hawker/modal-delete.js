adminApp.controller('Hawker.ModalDeleteController', function($scope, hawker, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.hawker = hawker;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      password: $scope.password
    };

    $http({
      url: base_url + "/hawker/"+$scope.hawker._id,
      method: "DELETE",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.hawker.name+' has successfully deleted.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.form['password'].$setValidity('server', false);
      serverErrors['password'] = data.message;

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});