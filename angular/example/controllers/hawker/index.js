adminApp.controller('Hawker.IndexController', function($scope, $http, $location, sharedProperties, ModalService) {

  var params = $location.search();

  var args = {
    sort: '-createdAt'
  };

  if (params.sort) {
    args.sort
      = $scope.sort
      = params.sort;

    $scope.sortOrder = params.sortOrder;
    if (params.sortOrder == 'desc') {
      args.sort = '-'+args.sort.replace('-', '');
    }
  }

  $scope.filterData = {};

  if (params.name) {
    args.name
      = $scope.filterData.name
      = params.name;
  }

  if (params.phoneNumber) {
    args.phoneNumber
      = $scope.filterData.phoneNumber
      = params.phoneNumber;

    if (args.phoneNumber.charAt(0) == '0') {
      args.phoneNumber = '+62'+args.phoneNumber.substring(1);
    }
  }

  if (params.verifiedByAdmin) {
    args.verifiedByAdmin
      = $scope.filterData.verifiedByAdmin
      = params.verifiedByAdmin;
  }

  if (params.prettyId) {
    args.prettyId
        = $scope.filterData.prettyId
        = params.prettyId;
  }

  // Paginate param
  var page = 1;
  if (params.page > 1) {
    page = params.page;
  }

  $http({
    method: "GET",
    url: base_url+"/hawkers/page/"+page,
    params: args,
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    var data = result;

    $scope.page = page;
    $scope.pageSize = data.limit;
    $scope.total = data.total;
    $scope.totalPage = Math.ceil(data.total/data.limit);
    $scope.hawkers = data.hawkers;

  });

  $scope.changeFilter = function () {
    delete params.page;
    delete params.prettyId;
    delete params.name;
    delete params.phoneNumber;
    delete params.verifiedByAdmin;

    if ($scope.filterData.prettyId) {
      params.prettyId = $scope.filterData.prettyId;
    }

    if ($scope.filterData.name) {
      params.name = $scope.filterData.name;
    }

    if ($scope.filterData.phoneNumber) {
      params.phoneNumber = $scope.filterData.phoneNumber;
    }

    if ($scope.filterData.verifiedByAdmin) {
      params.verifiedByAdmin = $scope.filterData.verifiedByAdmin;
    }

    if ($scope.filterData.verified) {
      params.verified = $scope.filterData.verified;
    }

    $location.search(params);
  };

  $scope.resetFilter = function () {
    delete params.page;
    delete params.name;
    delete params.phoneNumber;

    $location.search(params);
  };

  $scope.changeSort = function (sort) {
    delete params.page;
    
    if (params.sort != sort) {
      params.sort = sort;
      params.sortOrder = 'asc';
    } else {
      if (params.sortOrder && params.sortOrder == 'desc') {
        params.sortOrder = 'asc';
      } else {
        params.sortOrder = 'desc';
      }
    }

    $location.search(params);
  };

  $scope.changePage = function (text, page, pageSize, total) {
    params.page = page;
    $location.search(params);
  };

  // Action
  $scope.action = function (action, index) {
    var hawker = $scope.hawkers[index];

    switch (action) {
      case 'detail':
        sharedProperties.set('hawker', hawker);
        $location.path('/hawker/'+hawker._id).search({});
        break;
      case 'edit':
        sharedProperties.set('hawker', hawker);
        $location.path('/hawker/'+hawker._id+'/edit').search({});
        break;
      case 'delete':
        ModalService.showModal({
          templateUrl: 'views/hawker/modal-delete.html',
          controller: 'Hawker.ModalDeleteController',
          inputs: { hawker: hawker }
        });
        break;
      case 'verify':
        ModalService.showModal({
          templateUrl: 'views/hawker/modal-verify.html',
          controller: 'Hawker.ModalVerifyController',
          inputs: { hawker: hawker }
        });
        break;
      case 'trusted':
        ModalService.showModal({
          templateUrl: 'views/hawker/modal-update.html',
          controller: 'Hawker.ModalUpdateController',
          inputs: { hawker: hawker }
        });
        break;
    }
  };
  
});