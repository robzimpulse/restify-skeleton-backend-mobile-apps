adminApp.controller('Hawker.AddController', function($scope, $http, $location, cfpLoadingBar) {

  $scope.processing = false;
  $scope.hawker = {};
  $scope.serverErrors = {};

  var getCurrentYmd = function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    }

    return yyyy+mm+dd;
  };

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var data = $scope.hawker;

    $http({
      url: base_url+"/hawker",
      method: "POST",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/hawkers');

    }).error(function(data, status, headers, config) {

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (data.message.code == 11000) {
        var getField = message.errmsg.match(/hawkers.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  }
  
});