adminApp.controller('Hawker.ModalVerifyController', function($scope, hawker, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.hawker = hawker;
  $scope.serverErrors = {};

  $scope.verify = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {};

    $http({
      url: base_url + "/hawker/"+$scope.hawker._id+"/verify",
      method: "POST",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.hawker.name+' has successfully verified.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.form['password'].$setValidity('server', false);
      serverErrors['password'] = data.message;

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});