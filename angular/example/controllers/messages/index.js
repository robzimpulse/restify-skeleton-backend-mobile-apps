adminApp.controller('Message.IndexController', function($scope, $http, $location, sharedProperties, ModalService) {

  var params = $location.search();

  var args = {
    sort: '-createdAt'
  };

  if (params.sort) {
    args.sort
      = $scope.sort
      = params.sort;

    $scope.sortOrder = params.sortOrder;
    if (params.sortOrder == 'desc') {
      args.sort = '-'+args.sort.replace('-', '');
    }
  }

  $scope.filterData = {};

  if (params.event) {
    args.event
      = $scope.filterData.event
      = params.event;
  }
  if (params.message) {
    args.message
        = $scope.filterData.message
        = params.message;
  }

  // Paginate param
  var page = 1;
  if (params.page > 1) {
    page = params.page;
  }

  $http({
    method: "GET",
    url: base_url+"/messages/page/"+page,
    params: args,
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    var data = result;

    $scope.page = page;
    $scope.pageSize = data.limit;
    $scope.total = data.total;
    $scope.totalPage = Math.ceil(data.total/data.limit);
    $scope.messages = data.messages;

  });

  $scope.changeFilter = function () {
    delete params.event;
    delete params.message;

    if ($scope.filterData.event) {
      params.event = $scope.filterData.event;
    }

    if ($scope.filterData.message) {
      params.message = $scope.filterData.message;
    }

    $location.search(params);
  };

  $scope.resetFilter = function () {
    delete params.page;
    delete params.event;
    delete params.message;

    $location.search(params);
  };

  $scope.changeSort = function (sort) {
    delete params.page;
    
    if (params.sort != sort) {
      params.sort = sort;
      params.sortOrder = 'asc';
    } else {
      if (params.sortOrder && params.sortOrder == 'desc') {
        params.sortOrder = 'asc';
      } else {
        params.sortOrder = 'desc';
      }
    }

    $location.search(params);
  };

  $scope.changePage = function (text, page, pageSize, total) {
    params.page = page;
    $location.search(params);
  };

  // Action
  $scope.action = function (action, index) {
    var message = $scope.messages[index];

    switch (action) {
      case 'edit':
        sharedProperties.set('message', message);
        $location.path('/message/'+message._id+'/edit').search({});
        break;
      case 'delete':
        ModalService.showModal({
          templateUrl: 'views/message/modal-delete.html',
          controller: 'Message.ModalDeleteController',
          inputs: { message: message }
        });

        break;
    }
  };
  
});