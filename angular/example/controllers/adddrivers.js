adminApp.controller('AddDriversController', function($scope, $auth, $location, $http, Flash, localStorageService, Upload, $timeout) {
	
    // upload on file select 
    $scope.uploadFiles = function(file) {
        $scope.f = file;
        console.log(file);
        if (file && !file.$error) {
            file.upload = Upload.upload({
                url: '/ladyjek/admin-api/uploadPhoto/',
				headers: {
					'x-access-token': localStorageService.get('token'),
					'Content-Type': file.type
				},

                method: 'post',
                data: {files: file},
                file: file
            }).success(function (data, status, headers, config) {
            	console.log('file ' + config.file.name + 'uploaded. Response: ' + data.nameFile);
                $scope.photo = data.nameFile;
        	}).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            });

            file.upload.progress(function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }   
    }

	 $scope.addDriver = function () {
	 	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
		    dd='0'+dd
		} 
		if(mm<10) {
		    mm='0'+mm
		} 
		var dateNow = yyyy+mm+dd;

		var dataDriver = {
			'name': $scope.name,
			'email': $scope.email,
			'nomorKTP': $scope.nomorKTP,
			'nomorSIMC': $scope.nomorSIMC,
			'ktpBerlakuSampai': $scope.ktpBerlakuSampai,
			'simBerlakuSampai': $scope.simBerlakuSampai,
			'telpRumah': $scope.telpRumah,
			'phoneNumber': ($scope.phoneNumber.charAt(0) == '0' ? '+62'+$scope.phoneNumber.substring(1) : $scope.phoneNumber),
			'address': $scope.address,
			'rt': $scope.rt,
			'rw': $scope.rw,
			'kelurahan': $scope.kelurahan,
			'kecamatan': $scope.kecamatan,
			'kota': $scope.kota,
			'statusRumah': $scope.statusRumah,
			'statusNikah': $scope.statusNikah,
			'jumlahAnak': $scope.jumlahAnak,
			'usiaAnakBungsu': $scope.usiaAnakBungsu,
			'namaIbuKandung': $scope.namaIbuKandung,
			'tinggi': $scope.tinggi,
			'berat': $scope.berat,
			'kacamata': $scope.kacamata,
			'kacamataMinPlus': $scope.kacamataMinPlus,
			'pendidikan': $scope.pendidikan,
			'kontakDaruratNama': $scope.kontakDaruratNama,
			'kontakDaruratHubungan': $scope.kontakDaruratHubungan,
			'kontakDaruratTelpRumah': $scope.kontakDaruratTelpRumah,
			'kontakDaruratHP': $scope.kontakDaruratHP,
			'dikenalOleh': $scope.dikenalOleh,
			'idLadyJek': 'LJ' + dateNow + '00000',
			'dikenalID': $scope.dikenalID,
			'koordinatorLap': $scope.koordinatorLap,
			'idKopLap': $scope.idKopLap,
			'photo': $scope.photo,
			'vehicleNumber': '-',
			'emailRegistrant': localStorageService.get('admin').username,
			'x-access-token': 'sGMGdxuf4eBZgRE7346X90AkGUe4E9UF'
		}

		$http({
			url: "/ladyjek/admin-api/register1/",
			method: "POST",
			headers: {
				'x-access-token': localStorageService.get('token')
			},
			data: dataDriver
		}).success(function(data, status, headers, config) {
			$scope.data = data;
			$location.path('/drivers');
			// $http({
			// 	url: "/ladyjek/admin-api/log",
			// 	method: "POST",
			// 	headers: {
			// 		'x-access-token': localStorageService.get('token')
			// 	},
			// 	data: { 'id': $scope.data.id, 'working':'Add Driver : '+data.name, 'registrant': localStorageService.get('admin').username}
			// }).success(function(data, status, headers, config) {
			// 	$location.path('/drivers');
			// }).error(function(){
			// 	$location.path('/drivers');
			// });
		}).error(function(data, status, headers, config) {
			$scope.status = status;
            var errors = {};
            var first = true;
            for (var prop in data.message.errors) {
            	errors[prop] = data.message.errors[prop].message;
            	if (!first) {
            		document.getElementsByName(prop)[0].focus();
            		first = false;
            	}
            }
            $scope.errors = errors;
		});
		
	 }
});