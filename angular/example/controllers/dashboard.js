angular.module('adminApp').controller('DashboardController', function ($scope, $http, localStorageService, $location, sharedProperties) {

    $http({
        method: "GET",
        url: "/admin/dashboard/user",
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.userCount = data;

    });

    $http({
        method: "GET",
        url: "/admin/dashboard/hawker",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.hawkerCount = data;
    });

    $http({
        method: "GET",
        url: "/admin/dashboard/transaction",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.transactionCount = data;
    });

    $http({
        method: "GET",
        url: "/admin/dashboard/menu",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.menuCount = data;
    });

    $http({
        method: "GET",
        url: "/admin/dashboard/deliver/today",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.deliveredOrderTodayCount = data;
    });

    $http({
        method: "GET",
        url: "/admin/dashboard/deliver/tomorrow",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.deliveredOrderTomorrowCount = data;
    });

    $http({
        method: "GET",
        url: "/admin/dashboard/picked/tomorrow",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }

        var data = result;
        $scope.pickedMenuTodayCount = data;

    });

    // Action
    $scope.action = function (action, index) {
        switch (action) {
            case 'transactionDetail':
                var transaction = $scope.transactionCount.lists[index];
                sharedProperties.set('transaction', transaction);
                $location.path('/transaction/'+transaction._id+'/detail').search({});
                break;
            case 'orderTodayDetail':
                var transaction = $scope.deliveredOrderTodayCount.lists[index];
                sharedProperties.set('transaction', transaction);
                $location.path('/transaction/'+transaction._id+'/detail').search({});
                break;
            case 'userDetail':
                var user = $scope.userCount.lists[index];
                sharedProperties.set('user', user);
                $location.path('/user/'+user._id).search({});
                break;
            case 'hawkerDetail':
                var hawker = $scope.hawkerCount.lists[index];
                sharedProperties.set('hawker', hawker);
                $location.path('/hawker/'+hawker._id).search({});
                break;
            case 'orderTomorrowDetail':
                var transaction = $scope.deliveredOrderTomorrowCount.lists[index];
                sharedProperties.set('transaction', transaction);
                $location.path('/transaction/'+transaction._id+'/detail').search({});
                break;
            case 'menuDetail':
                var menu = $scope.menuCount.lists[index];
                $location.path('/menus').search({prettyId:menu.prettyId});
                break;
            case 'printTodayOrder':
                $location.path('print/delivery-schedule');
                break;
            case 'printTodayOrderText':
                $location.path('print/delivery-schedule-text');
                break;
            case 'printTomorrowOrder':
                $location.path('print/delivery-schedule-tomorrow');
                break;
            case 'printTomorrowOrderText':
                $location.path('print/delivery-schedule-tomorrow-text');
                break;
        }
    };

});

