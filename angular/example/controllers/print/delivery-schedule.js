adminApp.controller('Print.DeliveryScheduleController', function($scope, $routeParams, $http, $timeout) {

    $http({
        method: "GET",
        url: "/admin/dashboard/deliver/today",
        //params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        if (result.error) {
            Flash.create('danger', result.message);
        }
        var data = result;
        var today = new Date();
        today.setHours(0,0,0,0);
        data.today = today;

        var lists = data.lists;

        lists.forEach(function(list){
            var must_delete = [];
            list.orders.forEach(function(order,index,object){
                var tanggal = new Date(order.menu.openAt);
                tanggal.setHours(0,0,0,0);
                if(tanggal.toISOString() != today.toISOString()){
                    must_delete.push(index);
                }
            });
            console.log(must_delete);
            must_delete.forEach(function(id){
                list.orders.splice(id,1);
            });
        });

        $scope.deliveredOrderTodayCount = data;
        console.log(data);
        $timeout(function() {window.print();}, 3000);
    });



});