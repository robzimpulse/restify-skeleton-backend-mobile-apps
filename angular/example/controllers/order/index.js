adminApp.controller('Order.IndexController', function($scope, $route, $routeParams, $http, $location, Flash) {

  $scope.status = $routeParams.status;

  if ($scope.status == 'all') {
    $scope.status = '';
  }

  var params = $location.search();

  var args = {
    sort: '-createdAt'
  };

  if ($scope.status) {
    args.status = $scope.status;
  }

  if (params.sort) {
    args.sort
      = $scope.sort
      = params.sort;

    $scope.sortOrder = params.sortOrder;
    if (params.sortOrder == 'desc') {
      args.sort = '-'+args.sort.replace('-', '');
    }
  }

  $scope.filterData = {};

  if (params.idLadyJek) {
    args.idLadyJek
      = $scope.filterData.idLadyJek
      = params.idLadyJek;
  }

  if (params.from) {
    args.from
      = $scope.filterData.from
      = params.from;
  }

  if (params.to) {
    args.to
      = $scope.filterData.to
      = params.to;
  }

  if (params.includeCanceled) {
    args.includeCanceled
      = $scope.filterData.includeCanceled
      = params.includeCanceled;
  }

  // Paginate param
  var page = 1;
  if (params.page > 1) {
    page = params.page;
  }

  $http({
    method: "GET",
    url: "/ladyjek/admin-api/orders/page/"+page,
    params: args,
    headers: {
      'x-access-token': $scope.currentUser.token
    }
  }).success(function(result, status, headers, config) {

    if (result.error) {
      Flash.create('danger', result.message);
    }

    var data = result.data;

    $scope.page = page;
    $scope.pageSize = data.limit;
    $scope.total = data.total;
    $scope.totalPage = Math.ceil(data.total/data.limit);
    $scope.orders = data.orders;

  });

  $scope.changeFilter = function () {
    delete params.page;
    delete params.idLadyJek;
    delete params.from;
    delete params.to;
    delete params.includeCanceled;

    if ($scope.filterData.idLadyJek) {
      params.idLadyJek = $scope.filterData.idLadyJek;
    }

    if ($scope.filterData.from) {
      params.from = $scope.filterData.from;
    }

    if ($scope.filterData.to) {
      params.to = $scope.filterData.to;
    }

    if ($scope.filterData.includeCanceled) {
      params.includeCanceled = $scope.filterData.includeCanceled;
    }

    $location.search(params);
  };

  $scope.resetFilter = function () {
    delete params.page;
    delete params.idLadyJek;
    delete params.from;
    delete params.to;

    $location.search(params);
  };

  $scope.changeSort = function (sort) {
    delete params.page;
    
    if (params.sort != sort) {
      params.sort = sort;
      params.sortOrder = 'asc';
    } else {
      if (params.sortOrder && params.sortOrder == 'desc') {
        params.sortOrder = 'asc';
      } else {
        params.sortOrder = 'desc';
      }
    }

    $location.search(params);
  };

  $scope.changePage = function (text, page, pageSize, total) {
    params.page = page;
    $location.search(params);
  };

  $scope.refreshResult = function () {
    $route.reload();
  };

  $scope.exportResult = function () {

    $http({
      url: '/ladyjek/admin-api/orders/export',
      method: 'POST',
      responseType: 'arraybuffer',
      params: args,
      headers: {
        'x-access-token': $scope.currentUser.token
      }
    }).success(function (data, status, headers, config) {

      var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
      var objectUrl = URL.createObjectURL(blob);
      window.open(objectUrl);

    }).error(function (data, status, headers, config) {

      Flash.create('danger', 'Something Wrong. Please contact web administrator.');

    });

  };
  
});