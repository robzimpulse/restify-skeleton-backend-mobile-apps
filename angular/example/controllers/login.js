adminApp.controller('LoginController', function($scope, $auth, $location, Flash, localStorageService) {

  $auth.logout();

  $scope.username = '';
  $scope.password = '';
  
  $scope.login = function () {

    var credentials = {
      username: $scope.username,
      password: $scope.password
    }

    $auth.login(credentials)
      .then(function (response) {
        $scope.setCurrentUser(response.data.admin, response.data.token);

        if(response.data.admin.role == 'registrant'){
          $location.path('/drivers');
        } else if(response.data.admin.role == 'caller'){
          $location.path('/calls');
        } else if(response.data.admin.role == 'admin'){
          $location.path('/dashboard');
          $scope.fetchUnreadTrxCountStart();
        }
      })
      .catch(function () {
        Flash.create('danger', 'Sorry, invalid credentials', 'custom-class');
      });
  }

});