adminApp.controller('EditDriversController', function($scope, $routeParams, $auth, $location, $http, Flash, localStorageService, Upload, $timeout) {
	
	$scope.idDriver = $routeParams.id;

    // upload on file select 
    $scope.uploadFiles = function(file) {
        $scope.f = file;
        console.log(file);
        if (file && !file.$error) {
            file.upload = Upload.upload({
                url: '/ladyjek/admin-api/uploadPhoto/',
				headers: {
					'x-access-token': localStorageService.get('token'),
					'Content-Type': file.type
				},

                method: 'post',
                data: {files: file},
                file: file
            }).success(function (data, status, headers, config) {
            	console.log('file ' + config.file.name + 'uploaded. Response: ' + data.nameFile);
                $scope.photo = data.nameFile;
        	}).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            });

            file.upload.progress(function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }   
    }

	$http({
		url: "/ladyjek/admin-api/dataDrivers/searchID",
		method: "POST",
		headers: {
			'x-access-token': localStorageService.get('token')
		},
		data: {id : $routeParams.id}
	}).success(function(data, status, headers, config) {
		
		$scope.lists = data.data;
		$scope.name = data.data.name;
		$scope.email = data.data.email;
		$scope.nomorKTP = data.data.nomorKTP;
		$scope.nomorSIMC = data.data.nomorSIMC;
		$scope.ktpBerlakuSampai = data.data.ktpBerlakuSampai;
		$scope.simBerlakuSampai = data.data.simBerlakuSampai;
		$scope.telpRumah = data.data.telpRumah;
		$scope.phoneNumber = '0'+data.data.phoneNumber.toString().substr(3);
		$scope.address = data.data.address;
		$scope.rt = data.data.rt;
		$scope.rw = data.data.rw;
		$scope.kelurahan = data.data.kelurahan;
		$scope.kecamatan = data.data.kecamatan;
		$scope.kota = data.data.kota;
		$scope.statusRumah = data.data.statusRumah;
		$scope.statusNikah = data.data.statusNikah;
		$scope.jumlahAnak = data.data.jumlahAnak;
		$scope.usiaAnakBungsu = data.data.usiaAnakBungsu;
		$scope.namaIbuKandung = data.data.namaIbuKandung;
		$scope.tinggi = data.data.tinggi;
		$scope.berat = data.data.berat;
		$scope.kacamata = data.data.kacamata;
		$scope.kacamataMinPlus = data.data.kacamataMinPlus;
		$scope.pendidikan = data.data.pendidikan;
		$scope.kontakDaruratNama = data.data.kontakDaruratNama;
		$scope.kontakDaruratHubungan = data.data.kontakDaruratHubungan;
		$scope.kontakDaruratTelpRumah = data.data.kontakDaruratTelpRumah;
		$scope.kontakDaruratHP = data.data.kontakDaruratHP;
		$scope.dikenalOleh = data.data.dikenalOleh;
		$scope.dikenalID = data.data.dikenalID;
		$scope.koordinatorLap = data.data.koordinatorLap;
		$scope.idKopLap = data.data.idKopLap;
		$scope.photo = data.data.photo;

	}).error(function(data, status, headers, config) {
		console.log('Error');
	});

	 $scope.editDriver = function () {

		var dataDriver = {
			'id': $scope.idDriver || '',
			'name': $scope.name || '',
			'email': $scope.email || '',
			'nomorKTP': $scope.nomorKTP || '',
			'nomorSIMC': $scope.nomorSIMC || '',
			'ktpBerlakuSampai': $scope.ktpBerlakuSampai || '',
			'simBerlakuSampai': $scope.simBerlakuSampai || '',
			'telpRumah': $scope.telpRumah || '',
			'phoneNumber': ($scope.phoneNumber.charAt(0) == '0' ? '+62'+$scope.phoneNumber.substring(1) : $scope.phoneNumber) || '',
			'address': $scope.address || '',
			'rt': $scope.rt || '',
			'rw': $scope.rw || '',
			'kelurahan': $scope.kelurahan || '',
			'kecamatan': $scope.kecamatan || '',
			'kota': $scope.kota || '',
			'statusRumah': $scope.statusRumah || '',
			'statusNikah': $scope.statusNikah || '',
			'jumlahAnak': $scope.jumlahAnak || '',
			'usiaAnakBungsu': $scope.usiaAnakBungsu || '',
			'namaIbuKandung': $scope.namaIbuKandung || '',
			'tinggi': $scope.tinggi || '',
			'berat': $scope.berat || '',
			'kacamata': $scope.kacamata || '',
			'kacamataMinPlus': $scope.kacamataMinPlus || '',
			'pendidikan': $scope.pendidikan || '',
			'kontakDaruratNama': $scope.kontakDaruratNama || '',
			'kontakDaruratHubungan': $scope.kontakDaruratHubungan || '',
			'kontakDaruratTelpRumah': $scope.kontakDaruratTelpRumah || '',
			'kontakDaruratHP': $scope.kontakDaruratHP || '',
			'dikenalOleh': $scope.dikenalOleh || '',
			'dikenalID': $scope.dikenalID || '',
			'photo': $scope.photo || '',
			'koordinatorLap': $scope.koordinatorLap || '',
			'idKopLap': $scope.idKopLap || ''
		};

		$http({
			url: "/ladyjek/admin-api/dataDrivers/update",
			method: "POST",
			headers: {
				'x-access-token': localStorageService.get('token')
			},
			data: dataDriver
		}).success(function(data, status, headers, config) {
			$scope.data = data;
			$location.path('/drivers');
		}).error(function(data, status, headers, config) {
			console.log(data);
			$scope.status = status;
            var errors = {};
            var first = true;
            for (var prop in data.message.errors) {
            	errors[prop] = data.message.errors[prop].message;
            	if (!first) {
            		document.getElementsByName(prop)[0].focus();
            		first = false;
            	}
            }
            $scope.errors = errors;
		});
		
	 }
});