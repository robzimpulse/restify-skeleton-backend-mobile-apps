adminApp.controller('Coupon.ModalDeleteController', function($scope, coupon, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.coupon = coupon;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      _id: $scope.coupon._id
    };

    $http({
      url: base_url + "/coupon/"+$scope.coupon._id,
      method: "DELETE",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.coupon.name+' has successfully deleted.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});