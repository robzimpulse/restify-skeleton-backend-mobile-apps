adminApp.controller('Coupon.IndexController', function($scope, $http, $location, $log, sharedProperties, ModalService) {

    var params = $location.search();

    var args = {
        sort: '-createdAt'
    };

    if (params.sort) {
        args.sort
            = $scope.sort
            = params.sort;

        $scope.sortOrder = params.sortOrder;
        if (params.sortOrder == 'desc') {
            args.sort = '-'+args.sort.replace('-', '');
        }
    }

    $scope.filterData = {};

    if (params.name) {
        args.name
            = $scope.filterData.name
            = params.name;
    }

    if (params.type) {
        args.type
            = $scope.filterData.type
            = params.type;
    }

    if (params.code) {
        args.code
            = $scope.filterData.code
            = params.code;
    }

    if (params.value) {
        args.value
            = $scope.filterData.value
            = params.value;
    }

    if (params.oneTime) {
        args.oneTime
            = $scope.filterData.oneTime
            = params.oneTime;
    }

    // Paginate param
    var page = 1;
    if (params.page > 1) {
        page = params.page;
    }

    $http({
        method: "GET",
        url: base_url+"/coupons/"+page,
        params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        var data = result;

        $scope.page = page;
        $scope.pageSize = data.limit;
        $scope.total = data.total;
        $scope.totalPage = Math.ceil(data.total/data.limit);
        $scope.coupons = data.coupons;

    });

    $scope.changeFilter = function () {
        delete params.page;

        if ($scope.filterData.name) {
            params.name = $scope.filterData.name;
        }

        if ($scope.filterData.type) {
            params.type = $scope.filterData.type;
        }

        if ($scope.filterData.code) {
            params.code = $scope.filterData.code;
        }

        if ($scope.filterData.value) {
            params.value = $scope.filterData.value;
        }

        if ($scope.filterData.oneTime) {
            params.oneTime = $scope.filterData.oneTime;
        }

        if ($scope.filterData.isUsed) {
            params.isUsed = $scope.filterData.isUsed;
        }
        $log.log(params);
        $location.search(params);
    };

    $scope.resetFilter = function () {
        delete params.page;
        delete params.name;
        delete params.type;
        delete params.code;
        delete params.value;
        delete params.onetime;

        $location.search(params);
    };

    $scope.changeSort = function (sort) {
        delete params.page;

        if (params.sort != sort) {
            params.sort = sort;
            params.sortOrder = 'asc';
        } else {
            if (params.sortOrder && params.sortOrder == 'desc') {
                params.sortOrder = 'asc';
            } else {
                params.sortOrder = 'desc';
            }
        }

        $location.search(params);
    };

    $scope.changePage = function (text, page, pageSize, total) {
        params.page = page;
        $location.search(params);
    };

    // Action
    $scope.action = function (action, index) {
        var coupon = $scope.coupons[index];

        switch (action) {
            case 'edit':
                sharedProperties.set('coupon', coupon);
                $location.path('/coupon/'+coupon._id+'/edit').search({});
                break;
            case 'delete':
                ModalService.showModal({
                    templateUrl: 'views/coupon/modal-delete.html',
                    controller: 'Coupon.ModalDeleteController',
                    inputs: { coupon: coupon }
                });

                break;
        }
    };

});