adminApp.controller('Coupon.EditController', function($scope, $routeParams, $http, $location, sharedProperties, Flash) {

  $scope.processing = false;
  $scope.coupon = sharedProperties.get('coupon');
  
  if (!$scope.coupon || $scope.coupon._id != $routeParams.id) {
    $location.path('/coupons');
  }

  $scope.updateCouponName = $scope.coupon.name;
  $scope.serverErrors = {};

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var couponProperties = sharedProperties.get('coupon');
    var data = $.extend(couponProperties, $scope.coupon);

    $http({
      url: base_url+"/coupon/"+data._id,
      method: "PUT",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      Flash.create('success', $scope.coupon.name+': '+$scope.coupon.code+' has successfully updated.');
      $location.path('/coupons');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/coupons.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});