adminApp.controller('Faq.EditController', function($scope, $routeParams, $http, $location, sharedProperties) {

  $scope.processing = false;
  $scope.faq = sharedProperties.get('faq');
  
  if (!$scope.faq || $scope.faq._id != $routeParams.id) {
    $location.path('/faq');
  }

  $scope.updateFaqTitle = $scope.faq.title;
  $scope.serverErrors = {};

  $scope.save = function () {

    $scope.processing = true;

    var serverErrors = {};
    var faqProperties = sharedProperties.get('faq');
    var data = $.extend(faqProperties, $scope.faq);

    $http({
      url: base_url+"/faq/"+data._id,
      method: "PUT",
      headers: {
        'x-access-token': $scope.currentUser.token
      },
      data: data
    }).success(function(data, status, headers, config) {

      $location.path('/faq');

    }).error(function(data, status, headers, config) {
      
      var message = data.message;

      // Format error
      if (status == 422) {
        data.errors.forEach(function (error) {
          $scope.form[error.path].$setValidity('server', false);
          serverErrors[error.path] = error.message;
        });
      }

      // Duplicate error
      else if (message.code == 11000) {
        var getField = message.errmsg.match(/faqs.\$(.*)_1/);
        var field = getField[1];

        $scope.form[field].$setValidity('server', false);
        serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
      }

      $scope.serverErrors = serverErrors;
      $scope.processing = false;

    });

  };
  
});