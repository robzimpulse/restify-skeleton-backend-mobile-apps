adminApp.controller('Faq.AddController', function($scope, $http, $location,$log, cfpLoadingBar) {

    $scope.processing = false;
    $scope.faq = {};
    $scope.serverErrors = {};

    $scope.save = function () {

        $scope.processing = true;

        var serverErrors = {};
        var data = $scope.faq;
        $log.log(data);

        $http({
            url: "/admin/faq",
            method: "POST",
            headers: {
                'x-access-token': $scope.currentUser.token
            },
            data: data
        }).success(function(data, status, headers, config) {

            $location.path('/faq');

        }).error(function(data, status, headers, config) {

            var message = data.message;

            // Format error
            if (message.errors) {
                for (field in message.errors) {
                    $scope.form[field].$setValidity('server', false);
                    serverErrors[field] = message.errors[field].message;
                }
            }

            // Duplicate error
            else if (message.code == 11000) {
                var getField = message.errmsg.match(/drivers.\$(.*)_1/);
                var field = getField[1];

                $scope.form[field].$setValidity('server', false);
                serverErrors[field] = 'Data telah tersedia. Silahkan gunakan yang lain.';
            }

            $scope.serverErrors = serverErrors;
            $scope.processing = false;

        });

    }

});