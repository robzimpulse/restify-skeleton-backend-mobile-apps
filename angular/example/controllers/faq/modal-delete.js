adminApp.controller('Faq.ModalDeleteController', function($scope, faq, close, $http, $route, localStorageService, Flash) {

  $scope.processing = false;
  $scope.faq = faq;
  $scope.serverErrors = {};

  $scope.delete = function () {

    $scope.processing = true;
    
    var serverErrors = {};
    var data = {
      _id: $scope.faq._id
    };

    $http({
      url: base_url + "/faq/"+$scope.faq._id,
      method: "DELETE",
      headers: {
        'x-access-token': localStorageService.get('token')
      },
      data: data
    }).success(function(data, status, headers, config) {

      close();
      $route.reload();
      setTimeout(function () {
        Flash.create('success', $scope.faq.title+' has successfully deleted.');
      }, 100);

    }).error(function(data, status, headers, config) {
      
      $scope.processing = false;

    });

  };

  $scope.close = close;
  
});