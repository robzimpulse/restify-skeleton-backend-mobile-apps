adminApp.controller('Faq.IndexController', function($scope, $http, $location, $log, sharedProperties, ModalService) {

    var params = $location.search();

    var args = {
        sort: '-createdAt'
    };

    if (params.sort) {
        args.sort
            = $scope.sort
            = params.sort;

        $scope.sortOrder = params.sortOrder;
        if (params.sortOrder == 'desc') {
            args.sort = '-'+args.sort.replace('-', '');
        }
    }

    $scope.filterData = {};

    if (params.title) {
        args.title
            = $scope.filterData.title
            = params.title;
    }

    if (params.answer) {
        args.answer
            = $scope.filterData.answer
            = params.answer;
    }

    if (params.question) {
        args.question
            = $scope.filterData.question
            = params.question;
    }

    // Paginate param
    var page = 1;
    if (params.page > 1) {
        page = params.page;
    }

    $http({
        method: "GET",
        url: base_url+"/faqs/"+page,
        params: args,
        headers: {
            'x-access-token': $scope.currentUser.token
        }
    }).success(function(result, status, headers, config) {

        var data = result;

        $scope.page = page;
        $scope.pageSize = data.limit;
        $scope.total = data.total;
        $scope.totalPage = Math.ceil(data.total/data.limit);
        $scope.faqs = data.faqs;

    });

    $scope.changeFilter = function () {
        delete params.page;

        if ($scope.filterData.title) {
            params.title = $scope.filterData.title;
        }

        if ($scope.filterData.answer) {
            params.answer = $scope.filterData.answer;
        }

        if ($scope.filterData.question) {
            params.question = $scope.filterData.question;
        }

        $log.log(params);
        $location.search(params);
    };

    $scope.resetFilter = function () {
        delete params.title;
        delete params.answer;
        delete params.question;

        $location.search(params);
    };

    $scope.changeSort = function (sort) {
        delete params.page;

        if (params.sort != sort) {
            params.sort = sort;
            params.sortOrder = 'asc';
        } else {
            if (params.sortOrder && params.sortOrder == 'desc') {
                params.sortOrder = 'asc';
            } else {
                params.sortOrder = 'desc';
            }
        }

        $location.search(params);
    };

    $scope.changePage = function (text, page, pageSize, total) {
        params.page = page;
        $location.search(params);
    };

    // Action
    $scope.action = function (action, index) {
        var faq = $scope.faqs[index];

        switch (action) {
            case 'edit':
                sharedProperties.set('faq', faq);
                $location.path('/faq/'+faq._id+'/edit').search({});
                break;
            case 'delete':
                ModalService.showModal({
                    templateUrl: 'views/faq/modal-delete.html',
                    controller: 'Faq.ModalDeleteController',
                    inputs: { faq: faq }
                });

                break;
        }
    };

});