adminApp.directive('formProcessing', function () {

  return {
    restrict: 'A',
    link: function (scope, $elem) {

      scope.$watch(function () {
      	return scope.processing;
      }, function (processing) {

        $elem.find(':input').attr('disabled', processing);

      });

    }
  };

});