adminApp.directive('active', ['$location', function ($location) {

  return {
    restrict: 'A',
    link: function (scope, $elem, attrs) {

      var path = attrs.active;

      if ($location.path().substr(0, path.length) === path) {
        $elem.addClass('left-menu-active');
      } else {
        $elem.removeClass('left-menu-active');
      }
      
    }
  };

}]);