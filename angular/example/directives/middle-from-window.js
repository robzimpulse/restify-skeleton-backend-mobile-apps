adminApp.directive('middleFromWindow', ['$window', function ($window) {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {

      scope.$watch(function () {

        return {
          windowHeight: $window.innerHeight,
          elementHeight: element.prop('offsetHeight')
        };

      }, function (height) {
        
        var marginTop = (height.windowHeight - height.elementHeight) / 2;
        element.css('margin-top', marginTop+'px');

      }, true);

      $($window).resize(function () {
        scope.$apply();
      });

    }
  };

}]);