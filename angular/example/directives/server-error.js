adminApp.directive('serverError', ['$window', function ($window) {

  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, $element, attrs, ctrl) {

      var setTrue = function () {
        scope.$apply(function () {
          ctrl.$setValidity('server', true);
        });
      };

      $element.change(setTrue);
      $element.keyup(setTrue);

    }
  };

}]);