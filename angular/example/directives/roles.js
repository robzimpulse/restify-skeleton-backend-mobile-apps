adminApp.directive('roles', ['$auth', function ($auth) {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var roles = attrs.roles.split(',');

      var payload = $auth.getPayload();

      if (roles.indexOf(payload.role) == -1) {
        element.addClass('ng-hide');
      } else {
        element.removeClass('ng-hide');
      }
    }
  };

}]);