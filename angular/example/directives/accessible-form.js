adminApp.directive('accessibleForm', function ($timeout) {

  return {
    restrict: 'A',
    link: function (scope, $elem) {

    	scope.$watch(function () {
      	return scope.serverErrors;
      }, function (serverErrors) {

        if (serverErrors) {
        	for (var field in serverErrors) {
        		$elem.find('[name='+field+']').focus();
        		break;
        	}
        }

      });

    }
  };

});