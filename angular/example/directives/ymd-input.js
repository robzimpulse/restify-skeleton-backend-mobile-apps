adminApp.directive('ymdInput', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModelController) {
      ngModelController.$parsers.push(function(data) {
        //convert data from view format to model format
        if (!data) {
          return;
        }
        
        var dateComponents = data.split('-');
        var date = new Date(dateComponents[0], dateComponents[1]-1, dateComponents[2]);

        return date.toISOString(); //converted
      });

      ngModelController.$formatters.push(function(data) {
        //convert data from model format to view format
        if (!data) {
          return;
        }

        var date = new Date(data);
        return date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate()
      });
    }
  }
});