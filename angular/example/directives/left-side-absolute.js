adminApp.directive('leftSideAbsolute', ['$window', '$document', function ($window, $document) {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {

      var $leftSide = $('.left-side');

      scope.$watch(function () {

        var documentHeight = $document.outerHeight();

        return {
          documentHeight: documentHeight,
          leftSideHeight: $leftSide[0].scrollHeight
        };

      }, function (args) {

        var offsetTop = element.offset().top;
        var leftHeight = args.leftSideHeight + offsetTop;

        if (leftHeight == 0) {
          return;
        }

        if (leftHeight < args.documentHeight) {
          element.addClass('left-side-absolute');
        }

      }, true);

      $($window).resize(function () {
        scope.$apply();
      });

    }
  };

}]);