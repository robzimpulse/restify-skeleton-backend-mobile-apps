adminApp.directive('minWindowHeight', function ($window) {

  return {
    restrict: 'A',
    link: function (scope, $element, attrs) {

      scope.$watch(function () {

        return $window.innerHeight;

      }, function (windowHeight) {
        
        $element.css({minHeight:windowHeight+'px'});

      }, true);

      $($window).resize(function () {
        scope.$apply();
      });

    }
  };

});