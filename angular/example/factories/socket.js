adminApp.factory('socket', function (socketFactory) {
  return socketFactory({
    ioSocket: io.connect('https://ladyjek.com:2096/')
  });
});