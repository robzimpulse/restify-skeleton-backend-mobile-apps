adminApp.config(['$authProvider', function ($authProvider) {
  
  // Auth
  $authProvider.baseUrl = base_url;
  $authProvider.loginUrl = '/authenticate';

}]);