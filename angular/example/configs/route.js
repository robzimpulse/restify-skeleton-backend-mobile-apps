adminApp.config(['$routeProvider', function ($routeProvider) {
  
  // Routes
  $routeProvider
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginController'
    })
    .when('/dashboard', {
      templateUrl: 'views/dashboard.html',
      controller: 'DashboardController',
      roles: ['admin']
    })
    .when('/hawkers', {
      templateUrl: 'views/hawker/index.html',
      controller: 'Hawker.IndexController',
      roles: ['admin']
    })
    .when('/hawker/add', {
      templateUrl: 'views/hawker/form.html',
      controller: 'Hawker.AddController',
      roles: ['admin']
    })
    .when('/hawker/:id/edit', {
      templateUrl: 'views/hawker/form.html',
      controller: 'Hawker.EditController',
      roles: ['admin']
    })
    .when('/hawker/:id', {
      templateUrl: 'views/hawker/detail.html',
      controller: 'Hawker.DetailController',
      roles: ['admin']
    })
    .when('/menus', {
      templateUrl: 'views/menu/index.html',
      controller: 'Menu.IndexController',
      roles: ['admin']
    })
    .when('/menu/add', {
      templateUrl: 'views/menu/form.html',
      controller: 'Menu.AddController',
      roles: ['admin']
    })
    .when('/menu/:id/change-photo', {
      templateUrl: 'views/menu/change-photo.html',
      controller: 'Menu.ChangePhotoController',
      roles: ['admin']
    })
    .when('/menu/:id/edit', {
      templateUrl: 'views/menu/form.html',
      controller: 'Menu.EditController',
      roles: ['admin']
    })
    .when('/supplies', {
      templateUrl: 'views/supply/index.html',
      controller: 'Supply.IndexController',
      roles: ['admin']
    })
    .when('/supply/add', {
      templateUrl: 'views/supply/form.html',
      controller: 'Supply.AddController',
      roles: ['admin']
    })
    .when('/supply/:id/change-photo', {
      templateUrl: 'views/supply/change-photo.html',
      controller: 'Supply.ChangePhotoController',
      roles: ['admin']
    })
    .when('/supply/:id/edit', {
      templateUrl: 'views/supply/form.html',
      controller: 'Supply.EditController',
      roles: ['admin']
    })
    .when('/coupons', {
      templateUrl: 'views/coupon/index.html',
      controller: 'Coupon.IndexController',
      roles: ['admin']
    })
    .when('/coupon/add', {
      templateUrl: 'views/coupon/form.html',
      controller: 'Coupon.AddController',
      roles: ['admin']
    })
    .when('/coupon/:id/edit', {
      templateUrl: 'views/coupon/form.html',
      controller: 'Coupon.EditController',
      roles: ['admin']
    })
    .when('/faq', {
      templateUrl: 'views/faq/index.html',
      controller: 'Faq.IndexController',
      roles: ['admin']
    })
    .when('/faq/add', {
      templateUrl: 'views/faq/form.html',
      controller: 'Faq.AddController',
      roles: ['admin']
    })
    .when('/faq/:id/edit', {
      templateUrl: 'views/faq/form.html',
      controller: 'Faq.EditController',
      roles: ['admin']
    })
    .when('/transaction/:id/detail', {
      templateUrl: 'views/transaction/detail.html',
      controller: 'Transaction.DetailController',
      roles: ['admin']
    })
    .when('/transactions', {
      templateUrl: 'views/transaction/index.html',
      controller: 'Transaction.IndexController',
      roles: ['admin']
    })
    .when('/users', {
      templateUrl: 'views/user/index.html',
      controller: 'User.IndexController',
      roles: ['admin']
    })
    .when('/user/:id', {
      templateUrl: 'views/user/detail.html',
      controller: 'User.DetailController',
      roles: ['admin']
    })
    .when('/print/delivery-schedule', {
      templateUrl: 'views/print/delivery-schedule.html',
      controller: 'Print.DeliveryScheduleController',
      roles: ['admin']
    })
    .when('/print/delivery-schedule-text', {
      templateUrl: 'views/print/delivery-schedule-text.html',
      controller: 'Print.DeliveryScheduleController',
      roles: ['admin']
    })
    .when('/print/delivery-schedule-tomorrow', {
      templateUrl: 'views/print/delivery-schedule-tomorrow.html',
      controller: 'Print.DeliveryScheduleTomorrowController',
      roles: ['admin']
    })
    .when('/print/delivery-schedule-tomorrow-text', {
      templateUrl: 'views/print/delivery-schedule-tomorrow-text.html',
      controller: 'Print.DeliveryScheduleTomorrowController',
      roles: ['admin']
    })
    .otherwise({
      redirectTo: '/dashboard',
      roles: ['admin']
    });
}]);