adminApp.config(['$httpProvider', function ($httpProvider) {

  $httpProvider.interceptors.push(function($q, $location, Flash) {

    return {
      request: function($config) {
        if (!($config.url.indexOf("views/") > -1 && $config.url.indexOf(".html") > -1)) {
          $config.headers['Accept-Version'] = '~1';
          $config.headers['X-App-Token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlUb2tlbiI6IjdpLjdVMi9MQiwrSHtXS1hzeDAxMW5bJjlvSiQmaCIsImlhdCI6MTQ0ODAwOTE0NH0.S-Bj9K5oYUnJD9d3mYZP336h8Fi47vVLNmrCYXpQ0ds';
        }

        return $config;
      },
      'responseError': function(response) {
        if (response.status === 403) {
          $location.path('/login');
          Flash.create('danger', response.data.message, 'custom-class');
        }

        return $q.reject(response);
      }
    };

  });
  
}]);