adminApp.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {

  cfpLoadingBarProvider.startSize = 0.15;
  cfpLoadingBarProvider.latencyThreshold = 50;

}]);