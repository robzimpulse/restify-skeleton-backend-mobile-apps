adminApp.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
  
  // Local Storage
  localStorageServiceProvider.setPrefix('ladyjekAdminApp');
  
}]);