adminApp.service('ladyjekService', function($http, localStorageService) {

  return {

    generatePassdigit: function () {
      var high = 999999;
      var low = 100001;
      var passdigit = Math.floor(Math.random() * (high - low + 1) + low);

      return passdigit;
    },

    getNewLadyJekId: function (cb) {
      $http({
        url: "/ladyjek/admin-api/dataDrivers/id",
        method: "GET",
        headers: {
          'x-access-token': localStorageService.get('token')
        }
      }).success(function(data, status, headers, config) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        } 
        if(mm<10) {
            mm='0'+mm
        } 
        var dateNow = yyyy+''+mm+''+''+dd;
        var idSet = parseFloat(data.id.idSet)+1;
        var lengthID = idSet.toString().length;
        var idLady = 'LJ' + dateNow + '00000';
        var l = idLady.length-lengthID;
        var idLadyJek = idLady.substring(0, l)+idSet;
        
        return cb(false, {
          idSet: idSet,
          idLadyJek: idLadyJek
        });
      }).error(function(){
        return cb(true);
      });
    }

  };

});