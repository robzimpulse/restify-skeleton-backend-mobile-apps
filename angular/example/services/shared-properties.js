adminApp.service('sharedProperties', function(localStorageService) {

  var keyValue = localStorageService.get('keyValue') || {};

  keyValue['driver'] = {
    '_id': '',
    'name': '',
    'email': '',
    'namaIbuKandung': '',
    'dob': '',
    'nomorKTP': '',
    'nomorSIMC': '',
    'ktpBerlakuSampai': '',
    'simBerlakuSampai': '',
    'telpRumah': '',
    'phoneNumber': '',
    'address': '',
    'rt': '',
    'rw': '',
    'kelurahan': '',
    'kecamatan': '',
    'kota': '',
    'statusRumah': '',
    'statusNikah': '',
    'jumlahAnak': '',
    'usiaAnakBungsu': '',
    'tinggi': '',
    'berat': '',
    'kacamata': '',
    'kacamataMinPlus': '',
    'pendidikan': '',
    'kontakDaruratNama': '',
    'kontakDaruratHubungan': '',
    'kontakDaruratTelpRumah': '',
    'kontakDaruratHP': '',
    'dikenalOleh': '',
    'dikenalID': '',
    'photo': '',
    'koordinatorLap': '',
    'idKopLap': ''
  };
  
  return {
    get: function (key) {
      return keyValue[key];
    },
    set: function (key, value) {
      keyValue[key] = value;
      localStorageService.set('keyValue', keyValue);
    },
    remove: function (key) {
      localStorageService.remove(key);
    }
  }

});