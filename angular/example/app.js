var adminApp = angular.module('adminApp', ['ngRoute', 'satellizer', 'flash', 'ngAnimate', 'LocalStorageModule', 'ngFileUpload', 'btford.socket-io', 'bw.paging', 'vAccordion', 'ngMessages', 'angularModalService', 'angular-loading-bar', 'angular-img-cropper', 'ui.select','jkuri.datepicker']);

adminApp.run([
  '$rootScope',
  '$location',
  '$auth',
  'Flash',
  function ($rootScope, $location, $auth, Flash) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
      var roles = next.roles;

      if (!roles) {
        return;
      }

      if ( !$auth.isAuthenticated() ) {
        $location.path('/login');
        return;
      }

      var payload = $auth.getPayload();

      if (roles.indexOf(payload.role) == -1) {
        $auth.logout();
        $location.path('/login');
        Flash.create('danger', 'Forbidden', 'custom-class');
      }
    });
  }
]);