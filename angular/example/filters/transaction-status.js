adminApp.filter('transactionStatus', function() {

  return function(status) {

  	var statuses = {
  		waitingPayment: 'Waiting Payment',
  		verifyingPayment: 'Verifying Payment',
  		processing: 'Processing',
  		canceled: 'Canceled',
  		shipping: 'Shipping',
  		success: 'Success'
  	};

    return statuses[status];
  }

});