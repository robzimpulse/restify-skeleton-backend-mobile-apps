adminApp.filter('transactionType', function() {

  return function(type) {

  	var types = {
  		speed: 'Speed',
  		preorder: 'Preorder'
  	};

    return types[type];
  }

});