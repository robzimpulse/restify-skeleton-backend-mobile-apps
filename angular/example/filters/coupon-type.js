adminApp.filter('couponType', function() {

  return function(type) {

    var types = {
      discount: 'Discount',
      substract: 'Substract',
      free_shipping: 'Free Shipping',
      first_time_free: 'First Time Free'
    };

    return types[type];
  }

});