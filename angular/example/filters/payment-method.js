adminApp.filter('paymentMethod', function() {

  var methods = {
    cod: 'COD',
    transfer: 'Transfer',
    'e-cash': 'ECash'
  };

  return function(method) {

    return methods[method];

  }

});