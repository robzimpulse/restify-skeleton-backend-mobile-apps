adminApp.filter('gender', function() {

  return function(gender) {

    var genders = {
      male: 'Male',
      female: 'Female'
    };

    return genders[gender];
  }

});