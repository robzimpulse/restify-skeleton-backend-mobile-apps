adminApp.filter('orderStatus', function() {

  var statusText = {
    pending: 'Pending',
    accepted: 'Accepted',
    declined: 'Declined'
  };

  return function(status) {

    return statusText[status];

  }

});