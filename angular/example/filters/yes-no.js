adminApp.filter('yesNo', function() {

  return function(bool) {

    if (bool) {
      return 'Yes';
    } else {
      return 'No';
    }
  }

});