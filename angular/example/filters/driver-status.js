adminApp.filter('driverStatus', function() {

  return function(status) {

    if (status == 0) {
      return 'Inactive';
    } else {
      return 'Active';
    }
  }

});